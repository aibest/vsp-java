package com.aiwbest.service.impl;

import java.awt.*;

/**
 * <p></p>
 *
 * @author : sjx
 * @date : 2021-03-19 09:46
 **/
public class MyPanel {
    public static final int XPOINT = 100;
    public static final int YPOINT = 100;
    private double[] dataSource ; //存放数据的数组

    public MyPanel(double[] dataSource,Graphics g) {
        this.dataSource = dataSource;
        paintComponent(g);
    }

    /**
     * 生成对应图片
     * @param g
     */
    public void paintComponent(Graphics g) {
        g.setColor(Color.black);
        g.fillRect(0, 0, ImageIOHandler.WIDTH, ImageIOHandler.HEIGHT);//设置图片尺寸
        g.setColor(Color.CYAN);
//        g.drawLine(XPOINT, ImageIOHandler.HEIGHT - YPOINT, XPOINT, YPOINT);//画Y轴
//        g.drawLine(XPOINT,ImageIOHandler.HEIGHT/2, ImageIOHandler.WIDTH - XPOINT, ImageIOHandler.HEIGHT/2);//画X轴
        //储存曲线上所有点的x坐标
        int[] xx = new int[dataSource.length];
        //储存曲线上所有点的y坐标
        int[] yy = new int[dataSource.length];
        //对坐标数据进行赋值
        for (int i = 0; i < dataSource.length; i++) {
            xx[i] = i+XPOINT+500;//x轴坐标
            yy[i] = round(dataSource[i]);//y轴坐标,电压值
        }
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.red);//设置曲线颜色


        g2d.drawPolyline(xx, yy, dataSource.length);//画曲线
        g.dispose();//绘制图形
    }

    //将double类型转换为int类型,并对生成曲线的位置进行校正
    public int round(double data) {
        //先转化电压值,之后再对图像位置进行调整
        return ((int)(-data*500) + ImageIOHandler.HEIGHT/2);
    }
}
