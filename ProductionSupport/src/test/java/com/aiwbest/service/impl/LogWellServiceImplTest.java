package com.aiwbest.service.impl;

import com.aiwbest.pojo.LogWell;
import com.aiwbest.service.LogWellService;
import com.aiwbest.util.JavaCall;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;


/**
 * <p></p>
 *
 * @author : zlf
 * @date : 2021-03-02 14:17
 **/
@RunWith(SpringJUnit4ClassRunner.class)//表示让SpringJUnit4ClassRunner来运行
@ContextConfiguration(locations ={ "classpath:spring/spring-applicationContext.xml"})
public class LogWellServiceImplTest {


    @Autowired
    private LogWellService logWellService;
    @Autowired
    private JavaCall javaCall;
    @Test
    public void select() {
      List<LogWell> list = new  ArrayList<>(logWellService.select()) ;
        System.out.println(list.toString());
    }
    @Test
    public void selectById(){
        List<LogWell> list = new  ArrayList<>(logWellService.selectById("5")) ;
        System.out.println(list.toString());
    }
    @Test
    public void updataById(){
        LogWell logWell = new LogWell();
        double[] depth={23.23,25.52};
//        logWell.setYData(depth);
        logWell.setWellName("李四");
        logWellService.updataById(logWell);
    }
    @Test
    public void insert(){
        LogWell logWell = new LogWell();
        double[] depth={1.2,2.0};
//        logWell.setDepth(depth);
//        logWell.setYData(depth);
        logWell.setWellName("1002");
        logWell.setWellType("ac");
        logWellService.insert(logWell);
    }
    @Test
    public void test(){
        System.out.println("LogWellServiceImplTest.test");
        logWellService.delectById("");
    }
}