package com.aiwbest.service.impl;

import com.aiwbest.pojo.MyColor;
import com.aiwbest.pojo.SeismicDataLocation;
import com.aiwbest.pojo.colorMap;
import com.aiwbest.service.FileService;
import com.aiwbest.service.SeismicDataLocationService;
import com.aiwbest.service.SeismicIndex;
import com.aiwbest.util.JavaCall;
import com.aiwbest.util.Profile;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.imageio.ImageIO;
import javax.swing.plaf.IconUIResource;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.Assert.*;

/**
 * <p></p>
 *
 * @author : sjx
 * @date : 2021-03-10 15:46
 **/
@RunWith(SpringJUnit4ClassRunner.class)//表示让SpringJUnit4ClassRunner来运行
@ContextConfiguration(locations ={ "classpath:spring/spring-applicationContext.xml"})
public class SeismicLocationControllerServiceImplTest {

    @Autowired
    private SeismicDataLocationService seismicDataLocationService;
    @Autowired
    private SeismicIndex seismicIndex;
    @Autowired
    private FileService fileService;

    @Test
    public void select() {
        System.out.println(seismicDataLocationService.selectDefault().toString());
    }

    @Test
    public void sava() {
        SeismicDataLocation seismicDataLocation = new SeismicDataLocation();
        seismicDataLocation.setInLine(0);
        seismicDataLocation.setOffset(0);
        seismicDataLocation.setVearly(0);
        seismicDataLocation.setXLine(3);
        seismicDataLocation.setXSource(2);
        seismicDataLocation.setYSource(1);
        seismicDataLocationService.save(seismicDataLocation);
    }

    @Test
    public void test() {
//        System.out.println(seismicDataLocationService.selectDefault().toString());
        String fileName = "f3.segy";
        String path = "D:\\projectPojo\\survey\\三级 1-1-1\\KD101_NMO.sgy";
        seismicIndex.getSeismicIndex(seismicDataLocationService.selectOneByFileName(path));
    }

    @Test
    public void generateNo() {

        //接收流水号
        String generId = "";

        //生成5位随机数
        int radomInt = new Random().nextInt(99999);

        //获取系统当前时间

        SimpleDateFormat formatter = new SimpleDateFormat("yyMMddHHmm");
        Date date = new Date(System.currentTimeMillis());
        System.out.println(formatter.format(date));

        String dateInfo = formatter.format(date);

        //当前系统时分秒加上五位随机数,生成流水号
        generId = dateInfo + String.valueOf(radomInt);
        System.err.println(generId);

    }

    @Test
    public void test2() {
        seismicIndex.getSeismicData("f3.segy");
    }

    @Test
    public void CreatImg() {

        Profile profile = new Profile();


        //xLines,inLines,traceNum,traceID,allTrace,xLinNum,filePath
        float[] f = seismicIndex.getSeismicData("KD101_NMO.sgy");
        String fileName = "KD101_NMO.sgy";
        JavaCall javaCall = new JavaCall();
        int[] traceId =seismicIndex.getTraceId(fileName);
        int xStart = 10;
        int xEnd = 20;
        int inStart = 0;
        int inEnd = 2;
        int xLineLen = xEnd -xStart+1;
        int inLineLen = inEnd -inStart+1;
        int allTrace =traceId.length;


        float[] xLineList = new float[xLineLen];
        float[] inLineList = new float[inLineLen];
        for (int i = xStart,j = 0; i <= xEnd; i++,j++) {
            xLineList[j] = i;
        }

        for (int i = inStart,j = 0; i <= inEnd; i++,j++) {
            inLineList[j] = i;
        }
//        float[] yLineList = {200,201,2012,203,204};

//        float[] re=javaCall.readOneTraceData(100,"C:\\Users\\Administrator\\Desktop\\f3.segy");
        int traceNum =  inLineList.length*xLineList.length;
        float[] re = javaCall.ReadTraceData(xLineList,inLineList,traceNum,traceId,448,951,"C:\\Users\\Administrator\\Desktop\\f3.segy");

        float[] floats = profile.GetGray(re, traceNum, 800, 10800,0.5f,1,20);
        float[] getMaxAndMin = getMaxAndMin(floats);


         /*    */
        int w = 800;
        int h = 10800;
        int type = BufferedImage.TYPE_INT_RGB;

        BufferedImage image = new BufferedImage(w, h, type);

        int len = 0; // RGBA value, each component in a byte
        //定义贝尔塔
        int[] rgbList = new int[floats.length];
        for (int i = 0; i < floats.length; i++) {
            int b =(int)((floats[i]-getMaxAndMin[0])/(getMaxAndMin[1]-getMaxAndMin[0])*7-1);
            if (b <0)
                b = 0;
            ArrayList<MyColor> myColorList = testColor();
            int rgb = ((int)(myColorList.get(b).getR())<<16) + ((int)(myColorList.get(b).getG())<<8) + (int)(myColorList.get(b).getB());
//            System.err.println("rgb=" + rgb);
            rgbList[i] = rgb;
        }




        System.err.println(rgbList.length);
        for (int x = 0; x < w; x++) {
            for (int y = 0; y < h; y++) {

                image.setRGB(x, y, rgbList[len++]);
//                System.err.println(floats[len]);
            }
        }
        boolean val = false;
        String picType = "png";
        File file = new File("D:\\projectPojo\\survey\\三级 1-1-1\\4." + picType);

        try {
            val = ImageIO.write(image, picType, file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String imgPath = "D:\\projectPojo\\survey\\三级 1-1-1\\4.png";
        String imgData = "";
        try {
            FileInputStream fis = new FileInputStream(imgPath);
            int count = 0;
            while (count == 0){
                count = fis.available();
            }
            byte[] readImg = new byte[count];
            fis.read(readImg);
            imgData = Base64.getEncoder().encodeToString(readImg);
            System.err.println(imgData);
        }catch (Exception e){
            e.printStackTrace();
        }

        System.err.println(imgData);
    }
 public static ArrayList<MyColor> setColorMap(ArrayList<colorMap> map){
     float step = 1.0132f;
     double min;
     double max;
     MyColor minC ;
     MyColor maxC ;
     ArrayList<MyColor> outmap = new ArrayList<>();
     double aif ;
     for (double i = 0; i <= 1; i+=0.03125) {
         for (int j = 0; j < map.size()-1; j++) {
             if (i>=map.get(j).getX()&&i < map.get(j+1).getX()){
                 min = map.get(j).getX();
                 max = map.get(j+1).getX();
                 minC = map.get(j).getColor();
                 maxC = map.get(j+1).getColor();
                 aif = (i - min)/(max-min);
                 MyColor outColor = new MyColor();
                 outColor.setR((maxC.getR() - minC.getR())*aif+minC.getR());
                 outColor.setG((maxC.getG() - minC.getG())*aif+minC.getG());
                 outColor.setB((maxC.getB() - minC.getB())*aif+minC.getB());
                 outmap.add(outColor);
             }
         }
     }
     return outmap;
 }

    public static ArrayList<MyColor> testColor(){
     ArrayList<colorMap> mapList = new ArrayList<>();

         MyColor myColor1 = new MyColor();
         myColor1.setR(0);
         myColor1.setG(0);
         myColor1.setB(0);
         colorMap map1 = new colorMap();
         map1.setColor(myColor1);
         map1.setX(0);
         mapList.add(map1);

        MyColor myColor2 = new MyColor();
         colorMap map2 = new colorMap();
        myColor2.setR(128);
        myColor2.setG(138);
         myColor2.setB(135);

         map2.setColor(myColor2);
        map2.setX(0.2);
        mapList.add(map2);


//     MyColor myColor3 = new MyColor();
//     colorMap map3 = new colorMap();
//     myColor3.setR(255);
//     myColor3.setG(0);
//     myColor3.setB(0);
//
//     map3.setColor(myColor3);
//     map3.setX(1);
//     mapList.add(map3);
     ArrayList<MyColor> outmap = setColorMap(mapList);
     return outmap;
 }

 public static float[] getMaxAndMin(float[] array){
     float maxIndex = array[0];//定义最大值为该数组的第一个数
     float minIndex = array[0];//定义最小值为该数组的第一个数
     //遍历循环数组
     for (int i = 0; i < array.length; i++) {
         if(maxIndex < array[i]){
             maxIndex = array[i];
         }
         if(minIndex > array[i]){
             minIndex = array[i];
         }
     }
     float[] MinAndMax = new float[2];
     MinAndMax[0] = minIndex;
     MinAndMax[1] = maxIndex;
     return MinAndMax;
 }
 @Test
    public void getTraceId(){
     int[]  traceId = seismicIndex.getTraceId("f3.segy");
     System.err.println(traceId.length);
 }
}