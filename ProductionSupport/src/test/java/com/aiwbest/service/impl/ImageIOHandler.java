package com.aiwbest.service.impl;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * <p></p>
 *
 * @author : sjx
 * @date : 2021-03-19 09:47
 **/
public class ImageIOHandler {
    public static final int WIDTH = 5200;   //生成图片的宽度,单位是像素值
    public static final int HEIGHT = 1000;  //生成图片的高度,单位是像素值

    /**
     * 创建一个BufferedImage图片缓冲区对象并指定它宽高和类型 这样相当于创建一个画板，然后可以在上面画画
     */
    BufferedImage bi = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_BGR);

    /**
     * 要生成图片的类型,可以是JPG GIF JPEG PNG等...
     */
    final String picType = "png";

    /**
     * 成生成图片的保存路径和图片名称
     */
    final File file = new File("D:\\projectPojo\\survey\\三级 1-1-1\\1." + picType);

    /**
     * 通过指定参数写一个图片,并保存
     *
     * @param bi        图片缓冲流名称
     * @param picType   保存图片格式
     * @param file      保存图片的文件
     * @return boolean 如果失败返回一个布尔值
     */
    public static boolean writeImage(BufferedImage bi, String picType, File file) {
        //拿到画笔
        Graphics g = bi.getGraphics();

        //获取数据
        ECGData data = new ECGData();

        //实例化MyPanel对象,进行图片生成
        MyPanel myPanel = new MyPanel(data.getData(),g);

        boolean val = false;

        //输出为图片
        try {
            val = ImageIO.write(bi, picType, file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return val;
    }
}
