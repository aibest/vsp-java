package com.aiwbest.service.impl;

import com.aiwbest.pojo.WellDB;
import com.aiwbest.service.FileService;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;
import org.bson.types.ObjectId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.*;

/**
 * <p></p>
 *
 * @author : zlf
 * @date : 2021-03-03 11:28
 **/
@RunWith(SpringJUnit4ClassRunner.class)//表示让SpringJUnit4ClassRunner来运行
@ContextConfiguration(locations ={ "classpath:spring/spring-applicationContext.xml"})
public class FileDataServiceImplTest {

    @Autowired
    private FileService fileService;
    @Autowired
    private MongoTemplate mongoTemplate;
    @Test
    public void save2() throws IOException {
        HashMap map = new HashMap();
        GridFSInputFile file = fileService.saveFile("fff");
        if (file==null){
            map.put("msg","失败！！！");
        }else {
            map.put("msg","成功！！！");
        }
        System.out.println(map.toString());
    }
    @Test
    public void readFile() throws Exception{

        //1.连接服务器，创建实例
        GridFS gridFS = new GridFS(mongoTemplate.getDb());
        //文件是在DB基础上实现的，与表和文档没有关系

        //2.查找条件
        DBObject query = new BasicDBObject();
        List<GridFSDBFile> listFile = gridFS.find(query);
        GridFSDBFile gridFSDBFile = listFile.get(0);

        //3.获取文件名
        //注意：不是fs中的表的列名，而是根据调试gridDBFile中的属性而来
        String fileName = (String)gridFSDBFile.get("filename");
        System.out.println("从MongoDB获得的文件名为:"+fileName);

        //4.创建空文件
        File writeFile = new File("D:\\"+fileName);
        if(!writeFile.exists()){
            writeFile.createNewFile();
        }
        //5.写入文件
        gridFSDBFile.writeTo(writeFile);
    }
    @Test
    public void selecById(){
//        ObjectId str="604c1a6e4b66ae2fc807f6e2";
//        GridFSDBFile file =  fileService.selectById(str);
//        System.out.println(file.getFilename());
        //604b2a364b66ae358c77a6a7
        //604c1a6e4b66ae2fc807f6e2
    }
    @Test
    public void selecAll(){

        List<HashMap<String,Object>>  file = fileService.selectAll();
        System.out.println(file.toString());
    }

    @Test
    public void delecttById() {
        fileService.delecttById("ssm_mongodb.docx");
    }

    @Test
    public void findByName(){
        ArrayList<String> wellList = new ArrayList<>();

        wellList.add("GeoSed_f41_Curves.las");
        wellList.add("AC");
        wellList.add("DEPTH");

////        ArrayList re = fileService.findByName(wellList);
//        System.out.println(re);
//        re.add("123");
//        ArrayList wellDB =  fileService.findByName(new ArrayList());
//        LinkedHashMap linkedHashMap = (LinkedHashMap)wellDB.getWellDataList().get(0);
//        System.out.println(linkedHashMap.get("name"));
    }
    @Test
    public void findAll(){
        System.out.println(fileService.findAllFile());
    }
    @Test
    public void findMaxAndMin(){
       Map map = fileService.findMaxAndMin("f3.segy");
        System.err.println(map);
    }
}