package com.aiwbest.service.impl;

import com.aiwbest.util.JavaCall;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.net.ssl.HandshakeCompletedEvent;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * <p></p>
 *
 * @author : sjx
 * @date : 2021-03-09 14:37
 **/
@RunWith(SpringJUnit4ClassRunner.class)//表示让SpringJUnit4ClassRunner来运行
@ContextConfiguration(locations ={ "classpath:spring/spring-applicationContext.xml"})
public class WellListTwoServiceImplTest {

    @Test
    public void savaeAll() throws IOException {

        JavaCall javaCall = new JavaCall();
        ArrayList list = new ArrayList();
        String chars = javaCall.getTraceHeader(20, "fileName",4);
//        int[] header = new int[chars.length];
//        for (int i = 0; i < chars.length; i++) {
//             header[i] = Integer.valueOf(chars[i]);
//        }
        Map map = new HashMap();

    }
        @Test
        public void showURL() throws IOException {

            // 第一种：获取类加载的根路径   D:\git\daotie\daotie\target\classes
            File f = new File(this.getClass().getResource("/").getPath());
            System.out.println("path1: "+f);

            // 获取当前类的所在工程路径; 如果不加“/”  获取当前类的加载目录  D:\git\daotie\daotie\target\classes\my
            File f2 = new File(this.getClass().getResource("").getPath());
            System.out.println("path1: "+f2);

            // 第二种：获取项目路径    D:\git\daotie\daotie
            File directory = new File("");// 参数为空
            String courseFile = directory.getCanonicalPath();
            System.out.println("path2: "+courseFile);


            // 第三种：  file:/D:/git/daotie/daotie/target/classes/
            URL xmlpath = this.getClass().getClassLoader().getResource("");
            System.out.println("path3: "+xmlpath);

            // 第四种： D:\git\daotie\daotie
            System.out.println("path4:" +System.getProperty("user.dir"));

            /*
             * 结果： C:\Documents and Settings\Administrator\workspace\projectName
             * 获取当前工程路径
             */

            // 第五种：  获取所有的类路径 包括jar包的路径
            System.out.println("path5: "+System.getProperty("java.class.path").split(";")[0]);
            // 第六种：  获取项目路径  D:/git/daotie/daotie.target/classes/
            System.out.println("path6: "+Thread.currentThread().getContextClassLoader().getResource("").getPath());

            //第七种  表示到项目的根目录下, 要是想到目录下的子文件夹,修改"/"即可
//            HandshakeCompletedEvent request;
//            String path7 = request.getSession().getServletContext().getRealPath("/"));
//            System.out.pringln("path7: "+path7);
        }

        //返回异常数据；二维数组

}