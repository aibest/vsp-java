package com.aiwbest.controller;

import com.aiwbest.pojo.MyColor;
import com.aiwbest.pojo.colorMap;
import com.aiwbest.service.*;
import com.aiwbest.util.JavaCall;
import com.aiwbest.util.Profile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.bind.annotation.*;


import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;

/**
 * <p></p>
 *
 * @author : sjx
 * @date : 2021-03-22 11:57
 **/
@CrossOrigin
@Controller
@RequestMapping("img")
@ContextConfiguration(locations ={ "classpath:spring/spring-applicationContext.xml"})
public class GetImgController {
    @Autowired
    private getImgService getImgService;
    @Autowired
    private FileService fileService;
    @Autowired
    private SeismicIndex seismicIndex;
    @Autowired
    private ImgParameterService imgParameterService;
    @RequestMapping("/getImg")
    @ResponseBody
    public String getImg(String fileName,int xStart,int xEnd,int inStart,int inEnd,int timeStart,int timeEnd,int profileHeight, int profileWidth, float gain, int method, int size) {

        fileName = "KD101_NMO.sgy";
        System.out.println("fileName = " + fileName+"xStart = " +xStart);

        //存储用户的输入的值
//        String s = imgParameterService.saveImgPar(fileName,xStart,xEnd,inStart,inEnd,timeStart,timeEnd,profileHeight,profileWidth,gain,method,size);


        //获取fileName的filePath
//        fileName = "f3.segy";
        String filePath = fileService .getFileData(fileName);
        System.err.println("filePath =" +filePath);
//        traceId = 100;

        Profile profile = new Profile();

        int xLineLen = xEnd -xStart;
        int inLineLen = inEnd -inStart;
        int xLinNum = xLineLen;


        float[] xLineList = new float[xLineLen];
        float[] inLineList = new float[inLineLen];
        for (int i = xStart,j = 0; i < xEnd-1; i++,j++) {
            xLineList[j] = i;
        }

        for (int i = inStart,j = 0; i < inEnd-1; i++,j++) {
            inLineList[j] = i;
        }
        //xLines,inLines,traceNum,traceID,allTrace,xLinNum,filePath
//        float[] f = seismicIndex.getSeismicData(fileName);
        //获取traceId
        int traceNum =  inLineList.length*xLineList.length;
        int[] traceId =seismicIndex.getTraceId(fileName);
        int allTrace = traceId.length;


        xLinNum = 951;

        String imgPath = getImgService.CreatImg(fileName,filePath,xLineList,inLineList,traceNum,traceId,allTrace,xLinNum,profileHeight,profileWidth,gain,method,size);
        String imgData = "";
        try {
            FileInputStream fis = new FileInputStream(imgPath);
            int count = 0;
            while (count == 0){
                count = fis.available();
            }
            byte[] readImg = new byte[count];
            fis.read(readImg);
            imgData = Base64.getEncoder().encodeToString(readImg);
        }catch (Exception e){
            e.printStackTrace();
        }
        System.err.println(imgData);
        return imgData;
    }
}
