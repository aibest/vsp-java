package com.aiwbest.controller;

import com.aiwbest.pojo.projectPojo.AllProject;
import com.aiwbest.pojo.projectPojo.Survey;
import com.aiwbest.service.AllProjectService;
import com.aiwbest.util.RandomId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/project")
public class AllProjectController {
    @Autowired
    private AllProjectService allProjectService;

    /**
     * 新建工程
     * **/
    @RequestMapping("/insertProject")
    public Integer createProject(@RequestBody Map<String,String> map){
        System.out.println("请求成功");
        try {
            AllProject byProjectName = allProjectService.findProjectByProjectName(map.get("projectName"));
            if(byProjectName!=null){
                return 501;
            }
            AllProject allProject = new AllProject();
            allProject.setProjectName(map.get("projectName"));
            allProject.setProjectDescription(map.get("projectDescription"));
            allProject.setId(RandomId.generateNo());
            allProject.setCreateTime(new Date().toString());
            allProjectService.createProject(allProject);
            return 200;
        } catch (Exception e) {
            e.printStackTrace();
            return 500;
        }
    }

    /**
     * 根据工程名添加工区信息
     **/
    @RequestMapping("/insertSurvey")
    public Integer createSurveyByProjectName(@RequestBody Map<String,String> map) {
        Integer code = allProjectService.createSurveyByProjectName(map);
        return code;
    }

    /**
     * 根据工程名工区名删除工区信息
     **/
    @RequestMapping("/deleteSurvey")
    public Integer deleteSurveyByProjectNameAndSurveyName(@RequestBody Map<String,String> map){
        Integer code = allProjectService.deleteSurveyByProjectNameAndSurveyName(map);
        return code;
    }

    /**
     * 根据工程名工区名修改工区名
     * **/
    @RequestMapping("/updateSurvey")
    public Integer updateSurveyNameByProjectNameAndSurveyName(@RequestBody Map<String,String> map){
        Integer code = allProjectService.updateSurveyNameByProjectNameAndSurveyName(map);
        return code;
    }

    /**
     * 查询所有工程名
     **/
    @RequestMapping("/findAll")
    public List<String> findAllProject() {
        List<String> list = new ArrayList<>();
        List<AllProject> allProject = allProjectService.findAllProject();
        for (AllProject project : allProject) {
            list.add(project.getProjectName());
        }
        return list;
    }

    /**
     * 根据工程名查询全部工区名
     **/
    @RequestMapping("/findAllByProjectName")
    public List<String> findSurveyNameByProjectName(String projectName) {
        List<String> list = new ArrayList<>();
        AllProject byProjectName = allProjectService.findSurveyNameByProjectName(projectName);
        if (byProjectName == null) {
            list.add("501");
            return list;
        }
        if(byProjectName.getProjectChildren()==null){
            list.add("502");
            return list;
        }
        if(byProjectName.getProjectChildren().getSurvey()==null){
            list.add("502");
            return list;
        }
        ArrayList<Survey> survey = byProjectName.getProjectChildren().getSurvey();
        for (Survey survey1 : survey) {
            String surveyName = survey1.getSurveyName();
            list.add(surveyName);
        }
        return list;
    }
}
