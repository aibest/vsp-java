package com.aiwbest.controller;

import com.aiwbest.pojo.FileData;
import com.aiwbest.pojo.SeismicDataLocation;
import com.aiwbest.service.FileService;
import com.aiwbest.service.SeismicDataLocationService;
import com.aiwbest.service.SeismicIndex;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;


import com.sun.xml.internal.ws.encoding.ContentType;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Controller;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.zip.InflaterInputStream;

/**
 * <p>文件上传下载</p>
 *
 * @author : zlf
 * @date : 2021-03-05 10:39
 **/
@CrossOrigin
@Controller
@RequestMapping("file")
@ContextConfiguration(locations ={ "classpath:spring/spring-applicationContext.xml"})
public class FileController {
    @Autowired
    private FileService fileService;
    @Autowired
    private SeismicDataLocationService seismicDataLocationService;
    @Autowired
    private SeismicIndex seismicIndex;

//    private static final String DATA_DIR = System.getProperty("user.dir") + "/temp/";
    // 文件MD5的缓存容器
    private static final ConcurrentMap<String, File> MD5_CACHE = new ConcurrentHashMap<>();
    private static HashMap fileUpdata = new HashMap();
    private static long len;

    private static String locationFileName = "";
    private static HashMap<Integer,MultipartFile> muList = new HashMap<Integer,MultipartFile>();
    //文件上传
    @ResponseBody
    @RequestMapping("/updataLoadSgyFile")
    public HashMap chunkUpload(String name,
                            String md5,
                            Long size,
                            Integer chunks,
                            Integer chunk,
                            String path,
                            @RequestParam("file") MultipartFile multipartFile,HttpServletRequest request) throws IOException {
        // 是否生成了文件？？？
        muList.put(chunk,multipartFile);
        System.out.println(name);
        System.out.println(request.getCharacterEncoding());
        System.out.println(multipartFile.getContentType());

        String DATA_DIR = "D:\\"+path.replace("\"", "");
//        String DATA_DIR = "D:\\projectPojo\\survey\\三级 1-1-1";
        File targetFile = MD5_CACHE.get(md5);
        if (targetFile == null) {
            // 没有生成的话就生成一个新的文件，没有做并发控制，多线程上传会出问题
            targetFile = new File(DATA_DIR,name);
            targetFile.getParentFile().mkdirs();
            MD5_CACHE.put(md5, targetFile);
        }
        RandomAccessFile accessFile = new RandomAccessFile(targetFile, "rw");

        boolean finished = chunk == chunks;//是否最后一片
//
        String s1 = new String(multipartFile.getBytes());

        System.err.println("chunks = "+chunks+",size = "+size+",chunk"+chunk+",muSize="+multipartFile.getSize());
//        if (finished) {
//            // 移动指针到指定位置
//            accessFile.seek(size - multipartFile.getSize());
//        }else {
//            accessFile.seek((chunk - 1) * multipartFile.getSize());
//        }
////         写入分片的数据
//        synchronized (multipartFile){
//            accessFile.write(multipartFile.getBytes());
//            accessFile.close();
//        }



        if (finished) {
            System.out.println("success.");
            System.out.println(muList.get(1).getSize());
            System.out.println(muList.get(2).getSize());
            // 上传成功
            for (int i = 1; i <= muList.size(); i++) {
                if (i == muList.size()) {
                    // 移动指针到指定位置
                    accessFile.seek(size - multipartFile.getSize());
                }else {
                    accessFile.seek((chunk - 1) * multipartFile.getSize());
                }

                accessFile.write(muList.get(i).getBytes());

            }

            accessFile.close();
            //MultipartFile自带的解析方法
            String dbPath = targetFile.getPath();
            seismicIndex.getPath(dbPath);

            System.out.println(dbPath);


            //赋值给全局变量。
            locationFileName = dbPath;

            //上传文件到MongoDB中
            FileData fileData = new FileData();
            fileData.setFileName(name);
            fileData.setFilePath(dbPath);

//            String info = fileService.saveFileData(fileData);

            String info = "";
            System.out.println(fileData.getFilePath());

//            GridFSInputFile f = fileService.saveFile(dbPath);
            MD5_CACHE.remove(md5);
            if (info =="true"){
                fileUpdata.put("code",200);
                fileUpdata.put("msg","成功");
                fileUpdata.put("fileName",name);
                fileUpdata.put("defultData",seismicDataLocationService.selectDefault());
            }else {
                fileUpdata.put("code",402);
                fileUpdata.put("msg","上传失败！");
            }

        }
        return fileUpdata;
    }


    //获取初始值设置
    @ResponseBody
    @RequestMapping("/setPositionTable")
    public Map setPositionTable(@RequestBody SeismicDataLocation seismicDataLocation) throws IOException, ServletException {
        HashMap map = new HashMap();
        System.err.println(seismicDataLocation.toString());
        try {
//            seismicDataLocation.setFileName(locationFileName);
            seismicDataLocation.setFileName("D:\\projectPojo\\survey\\三级 1-1-1\\KD101_NMO.sgy");
            //存储索引位置数据
            seismicDataLocationService.save(seismicDataLocation);
            //解析文件,存放索引
            seismicIndex.getSeismicIndex(seismicDataLocation);
            //调用c++动态库
//            seismicIndex.getSeismicIndex(seismicDataLocation);
            map.put("code",200);
            map.put("msg","成功");
        }catch (Exception e){
            map.put("code",402);
            map.put("msg","上传失败！");
        }

        return map;
    }
    @RequestMapping("/getByteTable")
    @ResponseBody
    public Map getByteTable(Integer traceNumber, Integer traceId, String dataType,String fileName) throws IOException, ServletException {

        Map map = new HashMap();
        ArrayList list;
        fileName =  "C:\\Users\\Administrator\\Desktop\\f3.segy";
        try {
            list =fileService.getByte(traceNumber,traceId,dataType,fileName);
            map.put("data",list);
            map.put("code","200");
            map.put("msg","成功");
        }catch (Exception e){
            map.put("code","203");
            map.put("msg","失败");
        }finally {
            return map;
        }
    }

    @RequestMapping("/get2DLine")
    @ResponseBody
    public Map get2DLine(String fileName){
        Map map = new HashMap();
        try {
            List list = seismicDataLocationService.selectByFileName(fileName);
            map.put("data",list);
            map.put("code","200");
            map.put("msg","成功");
        }catch (Exception e){
            map.put("code","203");
            map.put("msg","失败");
        }finally {
            return map;
        }
    }

    //查询数据库中的所有文件名供用户选择。
    @RequestMapping("/getAllFile")
    @ResponseBody
    public Map getAllFile(){

        Map map = new HashMap();
        ArrayList<String> files = fileService.findAllFile();
        if (files!=null){
            map.put("fileName",files);
            map.put("msg","成功");
            map.put("code","200");
        }else {
            map.put("msg","失败");
            map.put("code","203");
        }
        return map;
    }

    //根据用户选择，返回相关数据。
    @RequestMapping("/getWellData")
    @ResponseBody
    public Map getWellData(String fileName,String logData){

        System.err.println("wellList = "+logData);
        System.out.println("fileName = "+fileName);
        String[] logDatas = logData.split(",");
        ArrayList<String> logDataList = new ArrayList<>();
        for (int i = 0; i < logDatas.length; i++) {
            logDataList.add(logDatas[i]);
        }
        //通过wellname寻找；
        ArrayList wellData = fileService.findByName(fileName,logDataList);

        //取出最大值和最小值。
        Map lineMaxAndMin = fileService.findMaxAndMin(fileName);
        lineMaxAndMin.put("wellData",wellData);

        //返回数据给前端
        if (wellData != null&&lineMaxAndMin!=null){
            lineMaxAndMin.put("msg","成功");
            lineMaxAndMin.put("code","200");
            return lineMaxAndMin;
        }else {
            Map map = new HashMap();
            map.put("msg","失败");
            map.put("code","203");
            return map;
        }
    }

    /*--------------------------------*/



    @RequestMapping("/updateFile")
    @ResponseBody
    public String updateFile(MultipartFile file,String path) throws IOException {
        System.out.println(file.getBytes());
        path = "";
        String s = "D:\\";
        File f1 = new File(s,file.getOriginalFilename());
        System.err.println(f1.getName());
        if(!f1.exists()){
            f1.mkdirs();
        }
        file.transferTo(f1);
        //MultipartFile自带的解析方法
        String dbPath = f1.getPath();
        System.out.println(dbPath);
        GridFSInputFile f = fileService.saveFile(dbPath);
        return "ok";
    }

    @ResponseBody
    @GetMapping("/selectById")
    public GridFSDBFile selecById(ObjectId fileId){
        GridFSDBFile file =  fileService.selectById(fileId);
        return file;
    }

    @ResponseBody
    @RequestMapping("/selectAll")
//    List<HashMap<String, Object>>
    public String selecAll(HttpServletRequest request, HttpServletResponse response) throws IOException {
        System.out.println(request.getParameter("name"));
        HashMap map = new HashMap();
        map.put("code",200);
        map.put("msg","查询成功");
        List<HashMap<String, Object>> file =  fileService.selectAll();
        System.out.println(file.toString());
        file.add(map);
        response.getWriter().write(String.valueOf(file));
        return "successful.jsp";
    }
    @RequestMapping("updataLoadSgyFile2")
    @ResponseBody
    public String loginUpload(MultipartHttpServletRequest request,String path) {
        // 获得第1张图片（根据前台的name名称得到上传的文件）
        MultipartFile file = request.getFile("file");

        String DATA_DIR = "D:\\"+path.replace("\"", "");

        if(file.getSize() != 0){
            // 获得文件名：
            String filename = file.getOriginalFilename();
            String timeType = null;
            if(null != filename && !filename.equals("")){
                String imgtype = filename.substring(filename.lastIndexOf("."));
                // 获取路径
                String ctxPath =DATA_DIR;
                // 创建文件
                File dirPath = new File(ctxPath);
                if (!dirPath.exists()) {
                    dirPath.mkdirs();
                }

                File uploadFile = new File(ctxPath + filename);
                System.out.println(uploadFile.getPath());
                try {
                    InputStream is = file.getInputStream();
                    FileOutputStream fos = new FileOutputStream(uploadFile);
                    int b = -1;
                    byte [] buffer = new byte[1024];

                    while((b = is.read(buffer)) != -1){
                        fos.write(buffer,0,b);
                    }
                    fos.close();
                    is.close();

                    //FileCopyUtils.copy(file.getBytes(), uploadFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return "redirect:/";
    }
    @RequestMapping("/tmp")
    public String tmp(){
        return "file";
    }

}
