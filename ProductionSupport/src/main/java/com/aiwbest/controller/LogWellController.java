package com.aiwbest.controller;

import com.aiwbest.pojo.*;
import com.aiwbest.service.LogWellService;
import com.aiwbest.service.WellListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * <p></p>
 *
 * @author : zlf
 * @date : 2021-03-02 14:38
 **/
@Controller
@CrossOrigin
@RequestMapping("logwell")
public class LogWellController {
    @Autowired
    private LogWellService logWellService;
    @Autowired
    private WellListService wellListService;

    //上传井数据，后端接收保存并返回反馈信息。
    @ResponseBody
    @RequestMapping("/setWellData")
    public HashMap test(@RequestBody WellListTest wellList){
        ArrayList<String> reqList = new ArrayList<>();
        int sizeOf = wellList.getWellName().size();
        System.err.println(wellList.getId().get(0));
        for (int i = 0; i < sizeOf ; i++) {
            WellList well = new WellList();
            well.setWellName(wellList.getWellName().get(i));
            well.setId(wellList.getId().get(i));
            well.setWellDataList(wellList.getWellDataList().get(i));
            well.setLog(wellList.getLog());
            well.setHeader(wellList.getHeader().get(i));
            System.out.println(well.getLog()+"==="+wellList.getLog());
//            System.out.println(well);
            reqList.add(wellListService.ParseData(well));
        }
        String req="";

        HashMap map = new HashMap();
        for (int i = 0; i < reqList.size(); i++) {
            if (!reqList.get(i).equals("successful")){
                map.put("code","203");
                map.put("msg","上传失败");
            }else {
                map.put("code","200");
                map.put("msg","上传成功");

            }
        }

        return map;
    }
//    @ResponseBody
//    @RequestMapping("/setWellData2")
//    public HashMap setWellData(@RequestBody WellList wellList){
//        System.out.println(wellList.getLog());
////        System.out.println(wellList);
////       String req = wellListService.ParseData(wellList);
//       String req = "";
//
//        HashMap map = new HashMap();
//        if (req.equals("successful")){
//            map.put("code","200");
//            map.put("msg","上传成功");
//        }else {
//            map.put("code","203");
//            map.put("msg","上传失败");
//        }
//        return map;
//    }
    /*----------------------------------*/
    //上传log数据
    @PostMapping("/setLogData")
    public HashMap setLogData(){
        HashMap map = new HashMap();
        if (true){

            map.put("code","200");
            map.put("msg","上传成功");

        }else {
            map.put("code","203");
            map.put("msg","上传失败");
        }
        return map;
    }


    /*-------------------------------*/
    @GetMapping("/select")
    public String select(){
        List<LogWell> list = logWellService.select();
        for(LogWell a :list){
            System.err.println("---------------"+a.getWellName()+a.getWellType());
        }
        return "admin/adminPage";
    }
    @RequestMapping("/add")
    public String adminFindAll(HttpServletRequest request){
     String s =   request.getParameter("wellName");
        return "admin/adminPage";
    }

}
