package com.aiwbest.controller;

import com.aiwbest.pojo.SeismicDataLocation;
import com.aiwbest.pojo.WellList;
import com.aiwbest.service.SeismicDataLocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * <p></p>
 *
 * @author : zlf
 * @date : 2021-03-08 15:16
 **/
@CrossOrigin
@Controller
@RequestMapping("/")
public class SeismicLocationController {

    @Autowired
    private SeismicDataLocationService seismicDataLocationService;

    //读取240字节数据表
    @RequestMapping("/getByteTable")
    @ResponseBody
    public ArrayList<float[]> getByteTable(int traceNumber, int traceId, String dataType,String fileName){

        //返回异常数据；二维数组
        return null;
    }
    //上传索引
    @ResponseBody
    @GetMapping("/setPositionTable")
    public void setPositionTable(HttpServletRequest request) throws IOException, ServletException {

    }
}
