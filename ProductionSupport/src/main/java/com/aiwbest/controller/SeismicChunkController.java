package com.aiwbest.controller;

import com.aiwbest.pojo.SeismicChunk;
import com.aiwbest.service.SeismicChunkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/seismic")
public class SeismicChunkController {
    @Autowired
    private SeismicChunkService seismicChunkService;

    @RequestMapping("/findPicTime")
    public float[] findPicTime(@RequestParam("dataId") String dataId){
        System.out.println("请求成功!");
        SeismicChunk picTime = seismicChunkService.findPicTime(dataId);
        if(picTime==null){
            return null;
        }
        return picTime.getPickTime();
    }
}
