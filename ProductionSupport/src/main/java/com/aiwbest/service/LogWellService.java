package com.aiwbest.service;

import com.aiwbest.pojo.LogWell;


import java.util.List;

/**
 * <p></p>
 *
 * @author : zlf
 * @date : 2021-03-02 11:08
 **/
public interface LogWellService {
    //查询所有
    public List<LogWell> select();
    //插入一个Logwell
    public void insert(LogWell logWell);
    //通过id查询
    public List<LogWell> selectById(String logName);
    //通过id修改
    public void updataById(LogWell logWell);
    void delectById(String logName);
}
