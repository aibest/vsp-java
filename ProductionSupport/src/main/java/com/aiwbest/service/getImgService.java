package com.aiwbest.service;

/**
 * <p>获取图片</p>
 *
 * @author : sjx
 * @date : 2021-03-26 14:10
 **/
public interface getImgService {
    String CreatImg(String fileName,String filePath,float [] xLines,float[] inLines,int traceNum, int[] traceId,int allTrace,int xLinNum,int profileHeight, int profileWidth, float gain, int method, int size);
}
