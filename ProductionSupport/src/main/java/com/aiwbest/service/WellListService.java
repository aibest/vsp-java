package com.aiwbest.service;

import com.aiwbest.pojo.WellDB;
import com.aiwbest.pojo.WellList;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>wellList</p>
 *
 * @author : zlf
 * @date : 2021-03-04 10:27
 **/
public interface WellListService {
    void saveList(WellDB wellDB);
    List<WellList> select(int id);

    String ParseData(WellList wellList);
}
