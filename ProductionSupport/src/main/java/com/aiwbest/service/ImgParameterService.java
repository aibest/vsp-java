package com.aiwbest.service;

/**
 * <p></p>
 *
 * @author : sjx
 * @date : 2021-03-29 20:15
 **/
public interface ImgParameterService {
    String saveImgPar(String fileName, int xStart, int xEnd, int inStart, int inEnd, int timeStart, int timeEnd, int profileHeight, int profileWidth, float gain, int method, int size);
}
