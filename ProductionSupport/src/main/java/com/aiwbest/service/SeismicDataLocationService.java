package com.aiwbest.service;

import com.aiwbest.pojo.SeismicDataLocation;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;

/**
 * <p></p>
 *
 * @author : zlf
 * @date : 2021-03-08 14:59
 **/
public interface SeismicDataLocationService {
    SeismicDataLocation selectDefault();
    void save(SeismicDataLocation seismicDataLocation);

    List<SeismicDataLocation> selectByFileName(String fileName);

    SeismicDataLocation selectOneByFileName(String path);
}
