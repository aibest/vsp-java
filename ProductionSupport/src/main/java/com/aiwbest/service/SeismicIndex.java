package com.aiwbest.service;

import com.aiwbest.pojo.SeismicDataLocation;
import com.aiwbest.util.JavaCall;

/**
 * <p>SeismicIndex</p>
 *
 * @author : sjx
 * @date : 2021-03-11 16:09
 **/
public interface SeismicIndex {
    String getPath(String path);
    float[] getData(float[] floats);
    float[] getSeismicIndex(SeismicDataLocation seismicDataLocation);
    float[] getSeismicData(String fileName);
    int getSeismicAllTrace(String fileName);

    int[] getTraceId(String fileName);
}
