package com.aiwbest.service;

import com.aiwbest.pojo.projectPojo.AllProject;

import java.util.List;
import java.util.Map;

/***
 * AllProject接口
 * */
public interface AllProjectService {
    //新建工程
    void createProject(AllProject allProject);
    //根据工程名查找工程信息
    AllProject findProjectByProjectName(String projectName);
    //根据工程名查找工程信息后新建工区
    Integer createSurveyByProjectName(Map<String,String> map);
    //根据projectName删除工程信息
    void deleteProjectByProjectName(String projectName);
    //根据工程名工区名删除工区
    Integer deleteSurveyByProjectNameAndSurveyName(Map<String,String> map);
    //根据工程名工区名修改工区名
    Integer updateSurveyNameByProjectNameAndSurveyName(Map<String,String> map);
    //查询所有工程名
    List<AllProject> findAllProject();
    //根据工程名查询全部工区名
    AllProject findSurveyNameByProjectName(String projectName);
}
