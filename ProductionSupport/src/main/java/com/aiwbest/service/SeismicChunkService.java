package com.aiwbest.service;

import com.aiwbest.pojo.SeismicChunk;

public interface SeismicChunkService {
    SeismicChunk findPicTime(String dataId);
}
