package com.aiwbest.service;

import com.aiwbest.pojo.FileData;
import com.aiwbest.pojo.WellDB;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;
import org.bson.types.ObjectId;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p></p>
 *
 * @author : zlf
 * @date : 2021-03-02 11:10
 **/

public interface FileService {
    void delecttById(String str);
    GridFSInputFile saveFile(String path) throws IOException;
    void updataFile(String path,String fileName) throws IOException;
    List<HashMap<String,Object>> selectAll();
    GridFSDBFile selectById(ObjectId fileId);
    GridFSDBFile selectByName(String name);

    ArrayList getByte(int traceNumber, int traceId, String dataType, String fileName);
    String saveFileData(FileData fileData);

    ArrayList findByName(String fileName,ArrayList<String> logoData);

    ArrayList<String> findAllFile();
    String getFileData(String fileName);

    Map findMaxAndMin(String s);
}
