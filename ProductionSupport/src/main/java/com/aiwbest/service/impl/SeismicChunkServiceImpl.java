package com.aiwbest.service.impl;

import com.aiwbest.pojo.SeismicChunk;
import com.aiwbest.service.SeismicChunkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;


@Service
public class SeismicChunkServiceImpl implements SeismicChunkService {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public SeismicChunk findPicTime(String dataId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("dataId").is(dataId));
        SeismicChunk seismicChunk = mongoTemplate.findOne(query, SeismicChunk.class);
        if(seismicChunk==null) {
            SeismicChunk seismicChunk1 = new SeismicChunk();
            float[] list = new float[100];
            for (int i = 0; i < 100; i++) {
                list[i] = 100 + i;
            }
            seismicChunk1.setPickTime(list);
            return seismicChunk1;
        }
        return seismicChunk;
    }
}
