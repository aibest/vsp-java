package com.aiwbest.service.impl;

import com.aiwbest.pojo.WellDB;
import com.aiwbest.pojo.WellList;
import com.aiwbest.service.WellListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * <p></p>
 *
 * @author : zlf
 * @date : 2021-03-04 10:27
 **/
@Service
public class WellListServiceImpl implements WellListService {

    @Autowired
    private MongoTemplate mongoTemplate;
    @Override
    public void saveList(WellDB wellDB) {
        mongoTemplate.insert(wellDB);
    }

    @Override
    public List<WellList> select(int id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(id));

        return mongoTemplate.find(query,WellList.class);
    }

    @Override
    public String ParseData(WellList wellList) {
        String req = "";
        ArrayList<String> wellDataList =wellList.getWellDataList();
        ArrayList<String> logData = wellList.getLog();
        ArrayList<Integer> t1 = new ArrayList<>();
        ArrayList<String> t2 = new ArrayList<>();
        ArrayList<String> t3 = new ArrayList<>();
        ArrayList<String> t4 = new ArrayList<>();


        for (int i = 0; i < logData.size(); i++) {
            ArrayList<String> loglist = new ArrayList();
            String[] strings = logData.get(i).split(" ");
            for (int j = 0; j < strings.length; j++) {
                if (!strings[j].equals("\t")){
                    loglist.add(strings[j]);
                }
            }

                t1.add(Integer.valueOf(loglist.get(0)));
                t2.add(loglist.get(1));
                t3.add(loglist.get(2));
                t4.add(loglist.get(3));

        }
        //获取头文件中的数据；

        /* -----------------数据存储---------------*/
        WellDB wellDB = new WellDB();
        wellDB.setWellName(wellList.getWellName());
        wellDB.setDepth(wellList.getDepth());
        wellDB.setHeader(Resolve(wellList.getHeader()));
        wellDB.setId(wellList.getId());
        wellDB.setLayer(wellList.getLayer());
        wellDB.setTime(wellList.getTime());

        ArrayList lists = new ArrayList();
        for (int i = 0; i < t1.size(); i++) {
            ArrayList arrayList = new ArrayList();
            for (int j = 0; j < wellDataList.size()-1; j++) {
                ArrayList<Float> list = new ArrayList();
                String[] arr = wellDataList.get(j).split(" ");
                for (int k = 0; k < arr.length; k++) {
                    if (!arr[k].equals("")){
                        list.add(Float.valueOf(arr[k]));
                    }
                }
                if (arr != null)
                    arrayList.add(list.get(t1.get(i)-1));
            }
            lists.add(arrayList);
        }
        /* -----------------wellDataList添加---------------------**/
        /*注意：用户每一排必须输入四个数，否者会报空指针异常*/
        ArrayList tt = new ArrayList();
        for (int i = 0; i < t1.size(); i++) {
            HashMap map = new HashMap();
            map.put("until",t4.get(i));
            map.put("data",lists.get(i));
            map.put("name",t2.get(i));
            map.put("alias",t3.get(i));
            tt.add(map);
        }
        wellDB.setWellDataList(tt);
        System.out.println(wellDB.toString());
        try {
            mongoTemplate.insert(wellDB);
            req = "successful";
        }catch (Exception e){
            e.printStackTrace();
            req = "fail";
        }
        return req;
    }

    //解析header文件

    public static Map Resolve(ArrayList<String> headerList){
        HashMap map = new HashMap();

        for (int i = 0; i < headerList.size()-2; i++) {
            ArrayList<String> transferList = new ArrayList<>();
            String[] strings = headerList.get(i).split(" ");
            //拿到所有的不为空的字符串，并解析字符串中含有数字的元素
            for (int j = 0; j < strings.length; j++) {
                    if (!strings[j].equals("")){
                        transferList.add(strings[j]);
//                        if (transferList.size() == 3){
//                            transferList.add(strings[j].split(":")[0]);
//                        }
                }
            }
            map.put(transferList.get(0),transferList.get(2));
        }
        System.out.println(map);
        return map;
    }
}
