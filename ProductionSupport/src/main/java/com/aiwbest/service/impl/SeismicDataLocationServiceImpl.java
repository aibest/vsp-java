package com.aiwbest.service.impl;

import com.aiwbest.pojo.SeismicDataLocation;
import com.aiwbest.service.SeismicDataLocationService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import org.springframework.data.mongodb.core.query.Query;

import java.util.ArrayList;
import java.util.List;

/**
 * <p></p>
 *
 * @author : zlf
 * @date : 2021-03-08 15:00
 **/
@Service
public class SeismicDataLocationServiceImpl implements SeismicDataLocationService {

    @Autowired
    private MongoTemplate mongoTemplate;
    @Override
    public SeismicDataLocation selectDefault() {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is("605076274b66ae1d3c600847"));
        return mongoTemplate.findOne(query,SeismicDataLocation.class);
    }

    @Override
    public void save(SeismicDataLocation seismicDataLocation) {
        mongoTemplate.insert(seismicDataLocation);
    }

    @Override
    public List<SeismicDataLocation> selectByFileName(String fileName) {
        Query query = new Query();
        query.addCriteria(Criteria.where("fileName").is(fileName));

        return mongoTemplate.find(query,SeismicDataLocation.class);
    }

    @Override
    public SeismicDataLocation selectOneByFileName(String path) {
        Query query = new Query();
        query.addCriteria(Criteria.where("fileName").is(path));
        return mongoTemplate.findOne(query,SeismicDataLocation.class);
    }
}
