package com.aiwbest.service.impl;

import com.aiwbest.pojo.MyColor;
import com.aiwbest.pojo.colorMap;
import com.aiwbest.service.SeismicDataLocationService;
import com.aiwbest.service.SeismicIndex;
import com.aiwbest.service.getImgService;
import com.aiwbest.util.JavaCall;
import com.aiwbest.util.Profile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * <p></p>
 *
 * @author : sjx
 * @date : 2021-03-26 14:10
 **/
@Service
public class GetImgServiceImpl implements getImgService {
    @Autowired
    private SeismicDataLocationService seismicDataLocationService;
    @Autowired
    private SeismicIndex seismicIndex;


    @Override
    public  String CreatImg(String fileName,String filePath,float [] xLines,float[] inLines,int traceNum, int[] traceId,int allTrace,int xLinNum,int profileHeight, int profileWidth, float gain, int method, int size){
        Profile profile = new Profile();


        //xLines,inLines,traceNum,traceID,allTrace,xLinNum,filePath
//        float[] f = seismicIndex.getSeismicData("f3.segy");//filePath


        JavaCall javaCall = new JavaCall();


        //获取一道的图片
//        float[] re=javaCall.ReadOneTraceData(traceId,filePath);
        //获取多道数据
        float[] re = javaCall.ReadTraceData(xLines,inLines,traceNum,traceId,allTrace,xLinNum,filePath);

//        float[] floats = profile.GetGray(re, 3, 650, 950,0.5f,1,20);
        float[] floats = profile.GetGray(re, traceNum, profileHeight, profileWidth,gain,method,size);
        float[] getMaxAndMin = getMaxAndMin(floats);
        /*    */
        int w = profileWidth;
        int h = profileHeight;
        int type = BufferedImage.TYPE_INT_RGB;

        BufferedImage image = new BufferedImage(w, h, type);

        int len = 0; // RGBA value, each component in a byte
        //定义贝尔塔
        int[] rgbList = new int[floats.length];
        for (int i = 0; i < floats.length; i++) {
            int b =(int)((floats[i]-getMaxAndMin[0])/(getMaxAndMin[1]-getMaxAndMin[0])*7-1);
            if (b <0)
                b = 0;
            ArrayList<MyColor> myColorList = GetColorList();
            int rgb = ((int)(myColorList.get(b).getR())<<16) + ((int)(myColorList.get(b).getG())<<8) + (int)(myColorList.get(b).getB());
            rgbList[i] = rgb;
        }
        for (int x = 0; x < w; x++) {
            for (int y = 0; y < h; y++) {
                image.setRGB(x, y, rgbList[len++]);
            }
        }
        boolean val = false;
        String picType = "png";
        File file = new File("D:\\projectPojo\\survey\\三级 1-1-1\\3." + picType);

        try {
            val = ImageIO.write(image, picType, file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return file.getPath();
    }

    public static ArrayList<MyColor> setColorMap(ArrayList<colorMap> map){
        float step = 1.0132f;
        double min;
        double max;
        MyColor minC ;
        MyColor maxC ;
        ArrayList<MyColor> outmap = new ArrayList<>();
        double aif ;
        for (double i = 0; i <= 1; i+=0.03125) {
            for (int j = 0; j < map.size()-1; j++) {
                if (i>=map.get(j).getX()&&i < map.get(j+1).getX()){
                    min = map.get(j).getX();
                    max = map.get(j+1).getX();
                    minC = map.get(j).getColor();
                    maxC = map.get(j+1).getColor();
                    aif = (i - min)/(max-min);
                    MyColor outColor = new MyColor();
                    outColor.setR((maxC.getR() - minC.getR())*aif+minC.getR());
                    outColor.setG((maxC.getG() - minC.getG())*aif+minC.getG());
                    outColor.setB((maxC.getB() - minC.getB())*aif+minC.getB());
                    outmap.add(outColor);
                }
            }
        }
        return outmap;
    }

    //获取color
    public static ArrayList<MyColor> GetColorList(){
        ArrayList<colorMap> mapList = new ArrayList<>();

        MyColor myColor1 = new MyColor();
        myColor1.setR(0);
        myColor1.setG(0);
        myColor1.setB(0);
        colorMap map1 = new colorMap();
        map1.setColor(myColor1);
        map1.setX(0);
        mapList.add(map1);

        MyColor myColor2 = new MyColor();
        colorMap map2 = new colorMap();
        myColor2.setR(128);
        myColor2.setG(138);
        myColor2.setB(135);

        map2.setColor(myColor2);
        map2.setX(0.2);
        mapList.add(map2);

        ArrayList<MyColor> outmap = setColorMap(mapList);
        return outmap;
    }

    //获取最大最小值
    public static float[] getMaxAndMin(float[] array){
        float maxIndex = array[0];//定义最大值为该数组的第一个数
        float minIndex = array[0];//定义最小值为该数组的第一个数
        //遍历循环数组
        for (int i = 0; i < array.length; i++) {
            if(maxIndex < array[i]){
                maxIndex = array[i];
            }
            if(minIndex > array[i]){
                minIndex = array[i];
            }
        }
        float[] MinAndMax = new float[2];
        MinAndMax[0] = minIndex;
        MinAndMax[1] = maxIndex;
        return MinAndMax;
    }
}
