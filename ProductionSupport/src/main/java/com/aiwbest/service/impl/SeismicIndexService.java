package com.aiwbest.service.impl;

import com.aiwbest.pojo.SeismicChunk;
import com.aiwbest.pojo.SeismicData;
import com.aiwbest.pojo.SeismicDataLocation;
import com.aiwbest.service.FileService;
import com.aiwbest.service.SeismicDataLocationService;
import com.aiwbest.service.SeismicIndex;
import com.aiwbest.util.JavaCall;
import com.aiwbest.util.RandomId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * <p></p>
 *
 * @author : sjx
 * @date : 2021-03-11 16:09
 **/
@Service
public class SeismicIndexService implements SeismicIndex{

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public  String getPath(String path) {
        return path;
    }

    @Override
    public float[] getData(float[] floats) {
        return floats;
    }
    @Autowired
    private FileService fileService;
    @Override
    public float[] getSeismicIndex(SeismicDataLocation seismicDataLocation) {
        System.out.println(seismicDataLocation.toString());
        float[] floats =
                {
                Float.valueOf(seismicDataLocation.getXLine()),Float.valueOf(seismicDataLocation.getStartTimeOrDepth()),
                Float.valueOf(seismicDataLocation.getInLine()),Float.valueOf(seismicDataLocation.getStartTimeOrDepth()),
                Float.valueOf(seismicDataLocation.getXSource()),Float.valueOf(seismicDataLocation.getStartTimeOrDepth()),
                Float.valueOf(seismicDataLocation.getYSource()),Float.valueOf(seismicDataLocation.getStartTimeOrDepth()),
                Float.valueOf(seismicDataLocation.getVearly()),Float.valueOf(seismicDataLocation.getStartTimeOrDepth()),
                Float.valueOf(seismicDataLocation.getOffset()),Float.valueOf(seismicDataLocation.getStartTimeOrDepth())
                };

        JavaCall javaCall = new JavaCall();
        String path =  seismicDataLocation.getFileName();
        System.err.println(path);

        path = "D:\\projectPojo\\survey\\三级 1-1-1\\KD101_NMO.sgy";

        float[] f = javaCall.readDF(path,floats);
        saveSeismicIndex(f,seismicDataLocation.getFileName());
        return f;
    }

    //索引数据分析、存储。
    public void saveSeismicIndex(float[] floats,String fileName){
        SeismicData seismicData = new SeismicData();
        SeismicChunk seismicChunk = new SeismicChunk();

        String id = new RandomId().generateNo();
        seismicData.setId(id);
        seismicData.setSample(floats[0]);
        seismicData.setType(floats[1]);
        seismicData.setInterval(floats[2]);
        seismicData.setAllTrace(floats[3]);
        seismicData.setXLineMin(floats[4]);
        seismicData.setXLineMax(floats[5]);
        seismicData.setInLinMin(floats[6]);
        seismicData.setInLinMax(floats[7]);
        seismicData.setFileName(fileName);

        mongoTemplate.insert(seismicData);
        int len =((int) floats[5] - (int) floats[4]+1)*((int) floats[7] - (int) floats[6]+1);

        int count = 1;

//        for (int i = 9,j = 0; i < len+9; i++) {
//            if(!Float.isNaN(floats[i])){
//                len--;
//            }
//        }

        //存储每一个小文件的存储数量
        List<Integer> num = new ArrayList<Integer>();
        //判断长度是否大于数据库最大长度，如果大于则进行分布式存储。
        if (len >130000){
            count = len/130000;
            for (int i = 0; i < count; i++) {
                num.add(130000);
                len = len-130000;
                if (len/130000 == 0){
                    num.add(len%130000);
                }
            }
        }


        //存放数据


        //元素的位置值；
        int index = 8;
        for (int i = 0; i <= count; i++) {
            float[] xS = new float[num.get(i)];
            for (int j = 0;j <num.get(i);) {
                if(!Float.isNaN(floats[index])){
                    xS[j] = floats[index++];
                }else {
                    xS[j] = -1f;
                }
                j++;
            }
            seismicChunk.setOffSet(xS);
            seismicChunk.setPickTime(xS);
            seismicChunk.setXSource(xS);
            seismicChunk.setYSource(xS);
            seismicChunk.setN(i+1);
            seismicChunk.setDataId(id);
            seismicChunk.setInLine(xS);
            seismicChunk.setTraceId(xS);
            seismicChunk.setXLine(xS);
            System.err.println("leng = "+xS.length);
            mongoTemplate.insert(seismicChunk);
        }
    }


    /*****取数据****/

    @Override
    public float[] getSeismicData(String fileName){
        //D:\projectPojo\survey\三级 1-1-1\232323.txt
//        String s = new File(fileName).getName();
        Query query = new Query();
        query.addCriteria(Criteria.where("fileName").is(fileName));
        SeismicData seismicData = mongoTemplate.findOne(query,SeismicData.class);
        String id = seismicData.getId();
        Query query2 = new Query();
        query2.addCriteria(Criteria.where("dataId").is(id));
        List<SeismicChunk> list = mongoTemplate.find(query2,SeismicChunk.class);

        //获取数据的长度
        int len = 0;
        for (int i = 0; i < list.size(); i++) {
            len = len+list.get(i).getXSource().length;
        }
        //获取数据
        float[] data = new float[len];
        for (int i = 0,j = 0,k=0; i <len ;i++,j++) {

            if (i%(list.get(k).getXSource().length-1) == 0&&i != 0){
                k++;
                if (k ==list.size())
                    break;
                j=0;
            }
            data[i] = list.get(k).getXSource()[j];

        }
        int x = data.length;

        return data;
    }

    @Override
    public int getSeismicAllTrace(String fileName) {
        Query query = new Query();
        query.addCriteria(Criteria.where("fileName").is(fileName));
        SeismicData seismicData = mongoTemplate.findOne(query,SeismicData.class);
        return (int) seismicData.getAllTrace();
    }

    @Override
    public int[] getTraceId(String fileName) {
        Query query = new Query();
        query.addCriteria(Criteria.where("fileName").is(fileName));
        SeismicData seismicData = mongoTemplate.findOne(query,SeismicData.class);
        String id = seismicData.getId();
        Query query2 = new Query();
        query2.addCriteria(Criteria.where("dataId").is(id));
        List<SeismicChunk> list = mongoTemplate.find(query2,SeismicChunk.class);

        //获取数据的长度
        int len = 0;
        for (int i = 0; i < list.size(); i++) {
            len = len+list.get(i).getTraceId().length;
        }
        //获取数据
        float[] data = new float[len];
        for (int i = 0,j = 0,k=0; i <len ;i++,j++) {

            if (i%(list.get(k).getTraceId().length-1) == 0&&i != 0){
                k++;
                if (k ==list.size())
                    break;
                j=0;
            }
            data[i] = list.get(k).getTraceId()[j];

        }
        int x = data.length;
        int[] traceId = new int[data.length];
        for (int i = 0; i < data.length; i++) {
            traceId[i] = (int) data[i];
        }
        return traceId;
    }

}
