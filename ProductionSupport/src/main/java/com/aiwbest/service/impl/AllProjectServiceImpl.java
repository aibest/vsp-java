package com.aiwbest.service.impl;

import com.aiwbest.pojo.projectPojo.AllProject;
import com.aiwbest.pojo.projectPojo.ProjectChildren;
import com.aiwbest.pojo.projectPojo.Survey;
import com.aiwbest.service.AllProjectService;
import com.aiwbest.util.RandomId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class AllProjectServiceImpl implements AllProjectService {
    @Autowired
    private MongoTemplate mongoTemplate;
    /**
     * 新建工程
     * */
    @Override
    public void createProject(AllProject allProject) {
        String id = RandomId.generateNo();
        allProject.setId(id);
        mongoTemplate.insert(allProject);
    }

    /**
     * 根据工程名查找该工程下所有信息
     * */
    @Override
    public AllProject findProjectByProjectName(String projectName) {
        Query query = new Query();
        query.addCriteria(Criteria.where("projectName").is(projectName));
        AllProject allProject = mongoTemplate.findOne(query, AllProject.class);
        if(allProject==null){
            return null;
        }
        return allProject;
    }


    /**
     * 根据工程名添加工区
     * **/
    @Override
    public Integer createSurveyByProjectName(Map<String,String> map) {
        try {
            AllProject projectByProjectName = findProjectByProjectName(map.get("projectName"));
            if(projectByProjectName==null){
                return 501;
            }
            Survey survey = new Survey();
            survey.setSurveyName(map.get("surveyName"));
            survey.setSurveyDescription(map.get("surveyDescription"));

            if(projectByProjectName.getProjectChildren()==null){
                ProjectChildren projectChildren = new ProjectChildren();
                ArrayList<Survey> surveyArrayList = new ArrayList<>();
                surveyArrayList.add(survey);
                projectChildren.setSurvey(surveyArrayList);
                projectByProjectName.setProjectChildren(projectChildren);
            }else {
                projectByProjectName.getProjectChildren().getSurvey().add(survey);
            }
            deleteProjectByProjectName(map.get("projectName"));
            mongoTemplate.insert(projectByProjectName);
            return 200;
        } catch (Exception e) {
            e.printStackTrace();
            return 500;
        }

    }

    /**
     * 根据工程名删除工程
     * **/
    @Override
    public void deleteProjectByProjectName(String projectName) {
        Query query = new Query();
        query.addCriteria(Criteria.where("projectName").is(projectName));
        mongoTemplate.findAndRemove(query,AllProject.class);
    }

    /**
     * 根据工程名工区名删除工区
     * 虽是删除，但实质是修改工程
     **/
    @Override
    public Integer deleteSurveyByProjectNameAndSurveyName(Map<String, String> map) {
        String projectName = map.get("projectName");
        String surveyName = map.get("surveyName");
        AllProject projectByProjectName = findProjectByProjectName(projectName);
        if(projectByProjectName==null){
            return 501;
        }
        if(projectByProjectName.getProjectChildren()==null){
            return 502;
        }
location:
            for (int i=0;i<projectByProjectName.getProjectChildren().getSurvey().size();i++) {
                if(projectByProjectName.getProjectChildren().getSurvey().get(i).getSurveyName().equals(surveyName)){
                    projectByProjectName.getProjectChildren().getSurvey().remove(i);
                    break location;
                }
        }
        deleteProjectByProjectName(projectName);
        mongoTemplate.insert(projectByProjectName);
        return 200;
    }

    /**
     * 根据工程名工区名修改工区名
     * **/
    @Override
    public Integer updateSurveyNameByProjectNameAndSurveyName(Map<String, String> map) {
        String projectName = map.get("projectName");
        String surveyName = map.get("surveyName");
        String newName = map.get("newName");
        AllProject projectByProjectName = findProjectByProjectName(projectName);
        if(projectByProjectName==null){
            return 501;
        }
        if(projectByProjectName.getProjectChildren()==null){
            return 502;
        }
        location:
            for (int i=0;i<projectByProjectName.getProjectChildren().getSurvey().size();i++) {
                if(projectByProjectName.getProjectChildren().getSurvey().get(i).getSurveyName().equals(surveyName)){
                    projectByProjectName.getProjectChildren().getSurvey().get(i).setSurveyName(newName);
                    break location;
                }
        }
        deleteProjectByProjectName(projectName);
        mongoTemplate.insert(projectByProjectName);
        return 200;
    }

    /**
     * 查询所有工程名
     * **/
    @Override
    public List<AllProject> findAllProject() {
        List<AllProject> projectList = mongoTemplate.findAll(AllProject.class);
        return projectList;
    }

    /**
     * 根据工程名查询全部工区名
     * **/
    @Override
    public AllProject findSurveyNameByProjectName(String projectName) {
        AllProject byProjectName = findProjectByProjectName(projectName);
        return byProjectName;
    }
}
