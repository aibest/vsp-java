package com.aiwbest.service.impl;

import com.aiwbest.pojo.LogWell;
import com.aiwbest.service.LogWellService;
import com.aiwbest.util.JavaCall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p></p>
 *
 * @author : zlf
 * @date : 2021-03-02 11:09
 **/
@Service
public class LogWellServiceImpl implements LogWellService {

    @Autowired
    private MongoTemplate mongoTemplate;
    @Override
    public List<LogWell> select() {
        return mongoTemplate.findAll(LogWell.class);
    }

    @Override
    public void insert(LogWell logWell) {

        mongoTemplate.insert(logWell);

    }

    @Override
    public List<LogWell> selectById(String logName) {
        Query query = new Query();
        query.addCriteria(Criteria.where("wellName").is(logName));
       return mongoTemplate.find(query,LogWell.class);
    }

    @Override
    public void updataById(LogWell logWell) {
        Query query = new Query();
        Update up = new Update();
//        up.set("yData",logWell.getYData());
        query.addCriteria(Criteria.where("wellName").is(logWell.getWellName()));
        int a = mongoTemplate.updateFirst(query,up,LogWell.class).getN();
    }

    @Override
    public void delectById(String logName) {

    }
}
