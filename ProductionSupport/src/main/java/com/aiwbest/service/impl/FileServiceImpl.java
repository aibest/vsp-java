package com.aiwbest.service.impl;

import com.aiwbest.pojo.FileData;
import com.aiwbest.pojo.SeismicData;
import com.aiwbest.pojo.Test;
import com.aiwbest.pojo.WellDB;
import com.aiwbest.service.FileService;
import com.aiwbest.util.JavaCall;
import com.aiwbest.util.test;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * <p></p>
 *
 * @author : zlf
 * @date : 2021-03-02 11:11
 **/
@Service
public class FileServiceImpl implements FileService {
    @Autowired
    private MongoTemplate mongoTemplate;


    @Override
    public void delecttById(String str) {
        GridFS gridFS = new GridFS(mongoTemplate.getDb(),"file");
        DBObject query  = new BasicDBObject("filename", str);

        gridFS.remove(query);
    }

    @Override
    public GridFSInputFile saveFile(String path) throws IOException {
        DB db = mongoTemplate.getDb();

        File file = new File(path);
        GridFS gridFS = new GridFS(db,"file");
        GridFSInputFile gridFSInputFile = gridFS.createFile(file);
        //判断文件名是否重复,如果重复，返回null;否则返回map集合
        HashMap map = new HashMap();
        if (selectByName(file.getName())!= null){
            return null;
        }else {
            gridFSInputFile.setFilename(file.getPath());
            gridFSInputFile.save();
            return gridFSInputFile;
        }
    }

    @Override
    public void updataFile(String path,String fileName) throws IOException {

        //1.连接服务器，创建实例

        //文件是在DB基础上实现的，与表和文档没有关系

        //2.查找条件
        GridFS gridFS = new GridFS(mongoTemplate.getDb());
        DBObject query = new BasicDBObject("filename", fileName);
        List<GridFSDBFile> listFile = gridFS.find(query);
        GridFSDBFile gridFSDBFile = gridFS.findOne(query);

        //3.获取文件名
        //注意：不是fs中的表的列名，而是根据调试gridDBFile中的属性而来
        System.out.println("从MongoDB获得的文件名为:"+fileName);

        //4.创建空文件
        File writeFile = new File(path+"\\"+fileName);
        if(!writeFile.exists()){
            writeFile.createNewFile();
        }
        //5.写入文件
        gridFSDBFile.writeTo(writeFile);
    }

    @Override
    public List<HashMap<String,Object>> selectAll() {
        GridFS gridFS = new GridFS(mongoTemplate.getDb(),"file");
        List<GridFSDBFile> result = gridFS.find((DBObject) null);
        List<HashMap<String, Object>> finalResult = new ArrayList<>();
        HashMap<String, Object> map = null;
        for (GridFSDBFile file : result) {
            map = new HashMap<>();
            map.put("id", ((ObjectId) file.getId()).toHexString());
            System.err.println("文件名"+file.getFilename());
            map.put("fileName", file.getFilename());
            map.put("length", file.getLength());
            finalResult.add(map);
        }
        return finalResult;
    }
    @Override
    public GridFSDBFile selectById(ObjectId fileId) {
        GridFS gridFS = new GridFS(mongoTemplate.getDb(),"file");
//        DBObject query  = new BasicDBObject("filename", str);

        GridFSDBFile file = gridFS.find(fileId);
        return file;
    }
    @Override
    public GridFSDBFile selectByName(String name) {
        GridFS gridFS = new GridFS(mongoTemplate.getDb(),"file");
        DBObject query  = new BasicDBObject("filename", name);

        GridFSDBFile file = gridFS.findOne(query);
        return file;
    }

    @Override
    public ArrayList getByte(int traceNumber, int traceId, String dataType, String fileName) {
        System.err.println(dataType);
        JavaCall javaCall = new JavaCall();
        ArrayList list = new ArrayList();
//        fileName = "C:\\Users\\Administrator\\Desktop\\f3.segy";
//        dataType = "4";

        for (int i = 0; i < traceNumber; i++) {
            System.out.println("-------------");
            int t_traceID = traceId+i;
            String f = javaCall.getTraceHeader(t_traceID,fileName,Integer.valueOf(dataType));
            String[] fs = f.split(",");
            float[] floats = new float[fs.length];
            for (int j = 0; j < fs.length; j++) {
                floats[j] = Float.valueOf(fs[j]);
            }
            list.add(floats);
        }
        return list  ;
    }

    @Override
    public String saveFileData(FileData fileData) {
        try {
            mongoTemplate.insert(fileData);
            return "true";
        }catch (Exception e){

            e.printStackTrace();
            return "fals";
        }

    }

    //获取测井数据
    @Override
    public ArrayList findByName(String fileName,ArrayList<String> logData) {
        Query query = new Query();
        query.addCriteria(Criteria.where("wellName").is(fileName));
        WellDB wellDB = mongoTemplate.findOne(query,WellDB.class);
        ArrayList reList = new ArrayList();        //存放返回数据
        for (int i = 0; i < logData.size(); i++) {
            reList.add(GeneratingData(wellDB,logData.get(i)));
        }
        return reList;
    }

    @Override
    public ArrayList<String> findAllFile() {
        ArrayList<String> fileList =new ArrayList<>();
        List<WellDB> list = mongoTemplate.findAll(WellDB.class);
        for (int i = 0; i < list.size(); i++) {
            fileList.add(list.get(i).getWellName());
        }
        return fileList;
    }

    @Override
    public String getFileData(String fileName) {
        Query query = new Query();
        query.addCriteria(Criteria.where("fileName").is(fileName));
        String filePath = mongoTemplate.findOne(query,FileData.class).getFilePath();
        return filePath;
    }

    //查找InLinMax，InLinMin
    @Override
    public Map findMaxAndMin(String s) {
        Query query = new Query();
        query.addCriteria(Criteria.where("fileName").is(s));
        SeismicData seismicData = mongoTemplate.findOne(query,SeismicData.class);
        float inMax = seismicData.getInLinMax();
        float inMin = seismicData.getInLinMin();
        float xMax = seismicData.getXLineMax();
        float xMin = seismicData.getXLineMin();
        Map map = new HashMap();
        map.put("InLinMax",inMax);
        map.put("InLinMin",inMin);
        map.put("XLineMax",xMax);
        map.put("XLineMin",xMin);
        return map;
    }


    //解析数据
    public static Map GeneratingData(WellDB wellDB,String attribute){
        float STEP = Float.parseFloat(wellDB.getHeader().get("STEP").toString());
        System.err.println(STEP);
        ArrayList<float[]> dealWithData = new ArrayList<>();//存放所有坐标的集合
        Map map = new HashMap();//存放返回数据。
        for (int i = 0; i < wellDB.getWellDataList().size(); i++) {
            LinkedHashMap linkedHashMap = (LinkedHashMap)wellDB.getWellDataList().get(i);
            if (linkedHashMap.get("name").equals(attribute)){
                ArrayList dataList =new  ArrayList((ArrayList)linkedHashMap.get("data"));
                for (int j = 0; j < dataList.size(); j++) {
                    float[] dot = new float[2];//用于存放x,y坐标;
                    float x =  Float.parseFloat(dataList.get(j).toString());
                    float result =x*STEP;
                    dot[0] = x;
                    dot[1] = result;
                    dealWithData.add(dot);

                }
                map.put(attribute,dealWithData);
            }
        }
        return map;
    }
}
