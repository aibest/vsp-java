package com.aiwbest.pojo;

import com.mongodb.DBObject;
import lombok.Data;
import org.bson.types.ObjectId;

/**
 * <p>地震数据道参数位置：所有数据皆代表在道头中的位置,字节数 </p>
 *
 * @author : zlf
 * @date : 2021-03-08 14:48
 **/
@Data
public class SeismicDataLocation {
    private ObjectId id;
    private String fileName;
    private float xLine;
    private float inLine;
    private float xSource;		//x坐标
    private float ySource;			//y坐标
    private float vearly;	//初置
    private float offset;//偏移道
    private float startTimeOrDepth;
}
