package com.aiwbest.pojo;

import lombok.Data;

/**
 * <p></p>
 *
 * @author : sjx
 * @date : 2021-03-26 09:39
 **/
@Data
public class MyColor {
    private double r;
    private double g;
    private double b;
}
