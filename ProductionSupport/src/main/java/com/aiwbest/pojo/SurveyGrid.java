package com.aiwbest.pojo;

import com.mongodb.DBObject;
import lombok.Data;
import org.bson.types.ObjectId;

/**
 * <p>测网数据</p>
 *
 * @author : zlf
 * @date : 2021-03-08 09:33
 **/
@Data
public class SurveyGrid {
    private String name;
    private int start;
    private int byteLen;
    private String dataType;

}
