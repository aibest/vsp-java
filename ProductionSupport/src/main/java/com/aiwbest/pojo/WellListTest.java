package com.aiwbest.pojo;

import lombok.Data;

import java.util.ArrayList;

/**
 * <p></p>
 *
 * @author : zlf
 * @date : 2021-03-03 16:12
 **/
@Data
public class WellListTest {
    private ArrayList<String> id;
    private ArrayList<String> wellName;
    private ArrayList<String> layer;
    private ArrayList<String> depth;
    private ArrayList<ArrayList<String>> header;
    private ArrayList<String> time;
    private ArrayList<String> log;
    private ArrayList<ArrayList<String>> wellDataList;
    private ArrayList<String> headerLog;
}
