package com.aiwbest.pojo;

import lombok.Data;

/**
 * <p></p>
 *
 * @author : sjx
 * @date : 2021-03-26 09:24
 **/
@Data
public class colorMap {
    private double x;
    private MyColor color;
}
