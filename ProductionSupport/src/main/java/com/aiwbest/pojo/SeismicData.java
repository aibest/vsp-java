package com.aiwbest.pojo;

import lombok.Data;

/**
 * <p>地震文件索引数据</p>
 *
 * @author : zlf
 * @date : 2021-03-05 15:51
 **/
@Data
public class SeismicData {
    private String id;
    private String fileName;
    private float sample;
    private float type;
    private float interval;
    private float allTrace;
    private float xLineMin;
    private float xLineMax;
    private float inLinMin;
    private float inLinMax;
    private float startTime;
}
