package com.aiwbest.pojo;

import lombok.Data;

import java.util.ArrayList;

/**
 * <p></p>
 *
 * @author : zlf
 * @date : 2021-03-03 16:12
 **/
@Data
public class WellList {
    private String id;
    private String wellName;
    private ArrayList<String> layer;
    private ArrayList<String> depth;
    private ArrayList<String> header;
    private String time;
    private ArrayList<String> log;
    private ArrayList<String> wellDataList;
}
