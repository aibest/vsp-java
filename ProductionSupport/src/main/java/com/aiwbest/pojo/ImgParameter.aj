package com.aiwbest.pojo;

import lombok.Data;

/**
 * <p></p>
 * @author : sjx
 * @date : 2021-03-29 20:11
 **/
@Data
public aspect ImgParameter {
    private String fileName;
    private int xStart;
    private int xEnd;
    private int inStart;
    private int inEnd;
    private int timeStart;
    private int timeEnd;
    private int profileHeight;
    private int profileWidth;
    private float gain;
    private int method;
    private int size;
}
