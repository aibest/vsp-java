package com.aiwbest.pojo;

import lombok.Data;
import org.bson.types.ObjectId;

/**
 * <p>分表</p>
 *
 * @author : sjx
 * @date : 2021-03-24 16:22
 **/
@Data
public class SeismicChunk {
    private String dataId;//父文件Id
    private int n;//文件位置
    private int index;//元素位置

    //存储数据
    private float[] xSource;
    private float[] ySource;
    private float[] pickTime;
    private float[] offSet;
    private float[] xLine;
    private float[] inLine;
    private float[] traceId;
}
