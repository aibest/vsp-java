package com.aiwbest.pojo;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * <p></p>
 *
 * @author : sjx
 * @date : 2021-03-15 20:43
 **/
@Data
public class WellDB {
    private String id;
    private String wellName;
    private ArrayList<String> layer;
    private ArrayList<String> depth;
    private Map header;
    private String time;
    private ArrayList wellDataList;
}
