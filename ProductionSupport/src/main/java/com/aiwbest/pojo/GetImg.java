package com.aiwbest.pojo;

import lombok.Data;

import java.io.File;

/**
 * <p></p>
 *
 * @author : zlf
 * @date : 2021-03-02 17:27
 **/
@Data
public class GetImg {
    private int imgId;
//    文件名字
    private String name ;

//    seismicData|beforeDealData|afterDealData|profileData
    private byte[] imgName;

//    增益
    private String gain;

//    显示模式，wiggle|color|gray
    private String displayMode;

//    显示模式agc|normal|weight|
    private String gainMode;

//    positive|negative|none
    private String wiggFile;

//    图片的像素宽度
    private double width;

//    图片的像素高度
    private double height;
}
