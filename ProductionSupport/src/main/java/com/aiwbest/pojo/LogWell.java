package com.aiwbest.pojo;

import com.sun.corba.se.spi.ior.ObjectId;
import lombok.Data;

/**
 * <p></p>
 *
 * @author : zlf
 * @date : 2021-03-02 11:04
 **/
@Data
public class LogWell {
    //井id

    private String wellName;
    //井的类型
    private String wellType;//
//    //井的深度
//    private double[] depth;
//    //井的纵坐标
//    private double[] yData;
}
