package com.aiwbest.pojo;

import lombok.Data;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * <p></p>
 *
 * @author : zlf
 * @date : 2021-03-04 09:40
 **/
@Data
public class WellData {
    private ArrayList<String> until;
    private ArrayList<float[]> dataList;
//    private Map DEPTH;
//    private Map AC;
//    private Map ac1;
//    private Map ac_cur;
//    private Map cali;
//    private Map cnl;
//    private Map den;
//    private Map den_cur;
//    private Map gr;
//    private Map imp_cur;
//    private Map ppp;
//    private Map ref_cur;
//    private Map ri;
//    private Map rt;
//    private Map rxo;
//    private Map sp;
//    private Map vel_cur;
}
