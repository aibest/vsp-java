package com.aiwbest.pojo;

import lombok.Data;

/**
 * <p>上传文件存储信息</p>
 *
 * @author : sjx
 * @date : 2021-03-18 16:16
 **/
@Data
public class FileData {
    private String filePath;
    private String fileName;
}
