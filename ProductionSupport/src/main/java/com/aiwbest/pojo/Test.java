package com.aiwbest.pojo;

import lombok.Data;

/**
 * <p></p>
 *
 * @author : sjx
 * @date : 2021-03-12 08:57
 **/
@Data
public class Test {
    private String wellName;
    private float age;
    private String name;
}
