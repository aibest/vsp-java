package com.aiwbest.pojo.projectPojo;

import lombok.Data;

import java.io.Serializable;

/**
 * 所有工程信息类
 * **/
@Data
public class AllProject implements Serializable {
    private String projectName;//工程名
    private String id;//工程Id
    private String projectDescription;//工程简介
    private String createTime;//工程创建时间
    private ProjectChildren projectChildren;//工程下的所有信息
}
