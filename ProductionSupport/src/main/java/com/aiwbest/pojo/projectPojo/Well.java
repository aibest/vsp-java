package com.aiwbest.pojo.projectPojo;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
/**
 * 所有井
 * **/
@Data
public class Well implements Serializable {
    private ArrayList<Layer> layer;
    private ArrayList<Log> log;
}
