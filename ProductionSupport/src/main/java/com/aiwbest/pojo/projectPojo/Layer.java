package com.aiwbest.pojo.projectPojo;

import lombok.Data;

import java.io.Serializable;

/**
 * 层位数据
 **/
@Data
public class Layer implements Serializable {
    private String layerName;//层位数据名
    private String layerUploadTime;//上传时间
}
