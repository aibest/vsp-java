package com.aiwbest.pojo.projectPojo;

import lombok.Data;

import java.io.Serializable;
/**
 * 井目录下所有信息
 * **/
@Data
public class SurveyChildren implements Serializable {
    private String SeismicName;//地震数据名
    private String Size;//地震数据大小
    private String seismicUploadTime;//上传时间
}
