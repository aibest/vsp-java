package com.aiwbest.pojo.projectPojo;

import lombok.Data;

import java.io.Serializable;
/**
 * 测井信息
 * **/
@Data
public class Log implements Serializable {
    private String logName;//测井文件名
    private String logUploadTime;//s上传时间
}
