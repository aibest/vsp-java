package com.aiwbest.pojo.projectPojo;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
/**
 * 工程下所有信息
 * **/
@Data
public class ProjectChildren implements Serializable {
    private ArrayList<Survey> survey;//所有工区信息
    private ArrayList<Well> well;//井目录
}
