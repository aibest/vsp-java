package com.aiwbest.pojo.projectPojo;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 所有工区
 * **/
@Data
public class Survey implements Serializable {
    private String surveyName;//工区名
    private String surveyDescription;//工区简介
    ArrayList<SurveyChildren> surveyChildren;//地震数据
}
