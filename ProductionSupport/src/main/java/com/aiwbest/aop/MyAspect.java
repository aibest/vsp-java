package com.aiwbest.aop;

import ch.qos.logback.core.joran.action.Action;
import org.aopalliance.aop.Advice;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

import java.util.Date;

/**
 * <p></p>
 *
 * @author : zlf
 * @date : 2021-03-02 11:01
 **/
@Aspect
public class MyAspect implements Advice {

    @Before(value = "execution(* com.aiwbest.service.impl.*.*(..))")
    public void MyBefor(){
        System.out.println("前置通知：在目标执行前输出时间："+new Date());
    }
    public void MyAftor(){

    }
}
