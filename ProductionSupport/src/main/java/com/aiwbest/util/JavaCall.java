package com.aiwbest.util;

import javax.xml.soap.SAAJResult;
import java.util.ArrayList;

/**
 * <p>java调度c++</p>
 *
 * @author : sjx
 * @date : 2021-03-03 09:43
 **/
public class JavaCall{
    public  native float[] readTracesData(float[] xLines,float[] inLines,int traceNum,int[] traceID, int allTrace,int xLinNum,String filePath);//traceNum和alltrace去除掉。
    private native float[] readDatafor(float[] bytyInfo,String filePath);//返回值是索引表数据
    public native float[] readOneTraceData(int traceID,String filePath);
    private native String getOneTraceHeader(String path,int traceID,int dataType);
    public native String getStringArray(String path);
    /*-------------*/


    //    加载dll
    static{
        try {
//            String dallPath =System.getProperty("user.dir")+"\\Data.dll";
////        System.out.println(dallPath);
//            System.load(dallPath);
//            System.loadLibrary("Data");
            System.out.println("开始加载！");
            System.load("D:\\workspace\\ProductionSupport\\Data.dll");
            System.out.println("加载成功！");
        }
        catch (Exception e){
            e.printStackTrace();
            System.out.println("文件没有加载进来");
        }
    }

    public static void main(String[] args) {
//        24，4，17，4，73，4，77，4，37，4，231，4
        float[] bytyInfo = {(float) 24.0,(float) 4.0,(float) 17.0,(float) 4.0,(float) 73.0,(float) 4.0,(float) 77.0,(float) 4.0,(float) 37.0,(float) 4.0,(float) 231.0,(float) 4.0,};
        float[] bytyInfo2 = {(float) 2.0,(float) 2.3};
        JavaCall javaCall = new JavaCall();
        String path = "C:\\Users\\Administrator\\Desktop\\f3.segy";
        String path3 = "C:\\Users\\Administrator\\Desktop\\柯东101井\\KD101_NMO.sgy";
        String path2 = "D:\\workspace\\ProductionSupport\\Seis_B.sgy";
//        String s = javaCall.getStringArray("");
        int traceId = 10;
        float[] f = javaCall.readDatafor(bytyInfo,path3);
        String s2 = javaCall.getOneTraceHeader(path,traceId,8);

//        System.out.println(s);
        System.err.println(f);
    }
    public  float[] readDF(String path,float[] floats){
        String dallPath =System.getProperty("user.dir")+"\\Data.dll";
        System.out.println("readDF :" +dallPath);
        System.load(dallPath);

        JavaCall javaCall = new JavaCall();
        System.out.println(path);
        System.out.println(path);
        float[] result = javaCall.readDatafor(floats,path);
        return  result;
    }

    public float[] readOTData(int traceID,String filePath){
        String dallPath =System.getProperty("user.dir")+"\\Data.dll";
        System.out.println("readOTData"+dallPath);
        System.load(dallPath);


        JavaCall javaCall = new JavaCall();
        float[] f = javaCall.readOneTraceData(traceID,filePath);
        System.out.println(f.length);
        return f;
    }
    public String testT(){
        System.out.println("这里是测试类！！！");
        return "chenggong!!!!";
    }
    public String getTraceHeader(int traceID,String filePath,int dataType){
        String dallPath =System.getProperty("user.dir")+"\\Data.dll";
        System.out.println(dallPath);
        System.load(dallPath);

        JavaCall javaCall = new JavaCall();

        return javaCall.getOneTraceHeader(filePath,traceID,dataType);
    }
       public float[] ReadTraceData(float[] xLines,float[] inLines,int traceNum,int[] traceID, int allTrace,int xLinNum,String filePath){

        JavaCall javaCall = new JavaCall();

        return javaCall.readTracesData(xLines,inLines,traceNum,traceID,allTrace,xLinNum,filePath);
    }
    public float[] ReadOneTraceData(int traceID, String filePath){
        String dallPath =System.getProperty("user.dir")+"\\Data.dll";
        System.load(dallPath);
        JavaCall javaCall = new JavaCall();

        return javaCall.readOneTraceData(traceID,filePath);
    }
}
