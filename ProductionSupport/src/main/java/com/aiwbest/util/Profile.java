package com.aiwbest.util;

import java.util.ArrayList;

/**
 * <p>生成剖面图</p>
 *
 * @author : sjx
 * @date : 2021-03-23 11:10
 * @
 **/
public class Profile {
    public native float[] getGray(float[] data, int traceNumber, int profileHeight, int profileWidth, float gain, int method, int size);
    public native float[] getColor(float[] data,int traceNumber,int profileHeight,int profileWidth);
    /*-------------*/

    //    加载dll
    static{
        try {
//            System.loadLibrary("Data");
            System.out.println("开始加载！");
            System.load("D:\\workspace\\ProductionSupport\\Data.dll");
            System.out.println("加载成功！");
        }
        catch (Exception e){
            e.printStackTrace();
            System.out.println("文件没有加载进来");
        }
    }

    public static void main(String[] args) {
//        String dallPath =System.getProperty("user.dir")+"\\Data.dll";
//        System.out.println(dallPath);
//        float[] data = {2.05f,3.03f,4.05f,6.6f,2.07f,3.032f,4.057f,6.62f,2.055f,3.035f,4.055f,6.65f,2.059f,3.039f,4.059f,6.69f};
        Profile profile = new Profile();
//        float[] result = profile.getColor(data,2,100,100);
//        for (int i = 0; i < result.length; i++) {
//            System.err.println(result[i]);
//        }
        float[] data = {2.05f,3.03f,4.05f,6.6f,2.07f,3.032f,4.057f,6.62f,2.055f,3.035f,4.055f,6.65f,2.059f,3.039f,4.059f,6.69f};

//        System.out.println(path);
//        path = "C:\\Users\\Administrator\\Desktop\\f3.segy";
//        System.out.println(path);
        float[] result = profile.getGray(data,1, 650, 950,1,1,1);
//        float[] result = profile.getColor(data,1, 650, 950);

    }
    public  float[] GetGray(float[] seismicData,int traceNumber,int profileHeight,int profileWidth,float gain,int method,int size){
//        String dallPath =System.getProperty("user.dir")+"\\seisVisData.dll";
//        System.out.println(dallPath);
//        System.load(dallPath);

        Profile profile = new Profile();
//        System.out.println(path);
        float[] result = profile.getGray(seismicData,traceNumber, profileHeight, profileWidth,gain,method,size);
//        float[] result2 = profile.getColor(seismicData,traceNumber, profileHeight, profileWidth);
        return  result;
    }

}
