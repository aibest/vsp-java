package com.aiwbest.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * <p>生成一个随机的唯一Id</p>
 *
 * @author : sjx
 * @date : 2021-03-24 16:43
 **/
public class RandomId {
    public static String generateNo(){

        //接收流水号
        String generId = "";

        //生成5位随机数
        int radomInt = new Random().nextInt(99999);

        //获取系统当前时间

        SimpleDateFormat formatter= new SimpleDateFormat("yyMMddHHmm");
        Date date = new Date(System.currentTimeMillis());
        System.out.println(formatter.format(date));

        String dateInfo = formatter.format(date);

        //当前系统时分秒加上五位随机数
        generId = dateInfo + String.valueOf(radomInt);
        return generId;
    }
}
