package com.aiwbest.util;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

/**
 * <p></p>
 *
 * @author : sjx
 * @date : 2021-03-15 14:03
 **/
public class test {

    public native String testOne();
//    public static void main(String[] args) {
//        try {
//            File f = new File("D:\\est.jpg");
//            if(!f.exists()){
//                f.mkdirs();
//            }
//            BufferedImage bi = ImageIO.read(f);
//            int width = bi.getWidth();
//            int height = bi.getHeight();
//            List<Integer> l = null;
//            Integer[] rbg = new Integer[width * height];
//            l = getRGB(bi, width, height);
//            for (int i = 0; i < width * height; i++) {
//                rbg[i] = l.get(i);
//            }
//            paint(rbg, width, height);
//        } catch (IOException ex) {
//            ex.printStackTrace();
//        }

//    }
public static void main(String[] args) throws Exception {
    RandomAccessFile ra = new RandomAccessFile("D:\\projectPojo\\survey\\WellList.java", "rw");
    ra.seek(0);
    ra.write("a bcd你好啊的撒法".getBytes());
    ra.seek(0);
//    System.out.println(new String(ra.readLine().getBytes("ISO-8859-1"),"utf-8"));//需要重新转码才能正常显示

    ra.close();

}

        public static List<Integer> getRGB(BufferedImage image, int x, int y) {
            List<Integer> rgb = new ArrayList<Integer>();
            if (image != null && x <= image.getWidth() && y <= image.getHeight()) {
                for (int h = 0; h < y; h++) {
                    for (int w = 0; w < x; w++) {
//获得w,h坐标的颜色
                        int pixel = image.getRGB(w, h);
                        rgb.add(pixel);
                    }
                }
            }
            return rgb;
        }
        public static void paint(Integer[] rbg, int width, int height) {
            File file = new File("D:/image.jpg");
            BufferedImage bi = new BufferedImage(width, height,
                    BufferedImage.TYPE_INT_RGB);
            Graphics2D g2 = (Graphics2D) bi.getGraphics();
            for (int h = 0; h < height; h++) {
                for (int w = 0; w < width; w++) {
                    int color = rbg[w + width * h];
                    Color c = new Color(color);
                    g2.setColor(c);
                    g2.drawLine(w, h, w + 1, h + 1);
                }
            }
            try {
                ImageIO.write(bi, "jpg", file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

}
