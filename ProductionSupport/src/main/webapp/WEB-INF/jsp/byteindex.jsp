<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2021/3/12 0012
  Time: 11:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>

<body>
<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript">
    $(function(){

        //alert(1);
        //这是json格式，但不是json串，这是key:value，相当于一个map。{}外面得加上一个单引号才是串，但只是个json格式的字符串，不是json数据
        //int traceNumber, int traceId, String dataType,String fileName
        var params = '{"traceNumber": 10,"traceId": 20,"dataType": 4,"fileName": ""}';
        // String fileName,int xStart,int xEnd,int inStart,int inEnd,int timeStart,int timeEnd,int profileHeight, int profileWidth, float gain, int method, int size
        var param = '{"fileName":"","xStart":300,"xEnd":600,"inStart":300,"inEnd":700,"timeStart":20,' +
            '"timeEnd":30,"profileHeight":650,"profileWidth":950,"gain":0.5,"method":1,"size":20,}'
        /*$.post(url,params,function(data){//这是回调json字符串，不能发送json字符串

        },"json");*/

        $.ajax({
            url: "${pageContext.request.contextPath }/img/getByteTable",
            data: params,
            contentType: "application/json;charset=UTF-8", //发送数据的格式
            type: "post",
            dataType: "json", //这是返回来是json，也就是回调json
            success: function(data){
                alert(data.name);
            }
        });
    });
</script>
</body>
</html>
