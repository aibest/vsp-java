<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2021/3/5 0005
  Time: 10:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>上传成功</title>
</head>
<body>
<div style="margin-left: 5%">
    <h3 style="text-align: center">文件列表</h3>
    <table border="1" onclick="findAll()">
        <thead>
        <tr style="background-color: beige">
            <td>文件名</td>
            <td>文件ID</td>
            <td>contentType</td>
            <td>文件大小</td>
            <td>上传时间</td>
            <td>md5</td>
            <td>操作</td>
        </tr>
        </thead>
        <tbody>
        <tr th:if="${files.size()} eq 0">
            <td colspan="3">没有文件信息！！</td>
        </tr>
        <tr th:each="file : ${files}">
            <td><a th:href="'selectAll/'+${file}" th:text="${file.name}" /></td>
            <td th:text="${file.id}" ></td>
            <td th:text="${file.contentType}" ></td>
            <td th:text="${file.size}" ></td>
            <td th:text="${file.uploadDate}" ></td>
            <td th:text="${file.md5}" ></td>
            <td><a target="_blank" th:href="@{/view(id=${file.id})}">预览</a>|<a th:href="@{/delete(id=${file.id})}">删除</a></td>
        </tr>
        </tbody>
    </table>

</div>
<script>
    function findAll() {
        $.ajax({
            url: "${pageContext.request.contextPath}/file/selectAll",
            type: "post",
            dataType: "json",
            success: function (data) {
                var html = "";
                for (var i = 0; i < data.length; i++) {
                }
                //html设置文本和标签
                $("#tb").html(html);
            }
        })
    }
</script>
</body>
</html>
