<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2021/3/12 0012
  Time: 11:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>

<body>
<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript">
    $(function(){
        //alert(1);
        // private int id;
        // private String wellName;
        // private ArrayList<String> layer;
        // private ArrayList<String> depth;
        // private wellHeader header;
        // private String time;
        // private ArrayList<String> log;
        // private ArrayList<String> wellDataList;
        //这是json格式，但不是json串，这是key:value，相当于一个map。{}外面得加上一个单引号才是串，但只是个json格式的字符串，不是json数据
        var list = [];
        list[0] = "2230    6533    5899   6666   488   5855";
        list[1] = "111   2222  33333  88888  9999";
        list[2] = "88   1002  1003  8828  1000.0";
        var loglist =[]
        loglist[0] = "1\tac\tac\tM"
        loglist[1] = "2\tDE\tDE\tS"

        loglist[2] = "3\tS\tob\tm/s"


        var wellList = {wellName:"f1",id:2,layer:["a","b"],depth:["aa","bb"],time:"-1",log:loglist,wellDataList:list};
        // String s1 = "2230    6533    5899   6666   488   5855";
        // String s2 = "111   2222  33333  88888  9999";
        // String fileName,int xStart,int xEnd,int inStart,int inEnd,int timeStart,int timeEnd,int profileHeight,
        // int profileWidth, float gain, int method, int size
        var data = {fileName:"",xStart:300,xEnd:600,inStart:300,inEnd:700,timeStart:20,
            timeEnd:30,profileHeight:650,profileWidth:950,gain:0.5,method:1,size:20}
        /*$.post(url,params,function(data){//这是回调json字符串，不能发送json字符串

        },"json");*/
        var user = {
            "fileName": "hahah",
            "password": "123456"
        };

        $.ajax({
            url: "${pageContext.request.contextPath }/img/getImg",
            data: {fileName:"KD101_stack.sgy",xStart:0,xEnd:20,inStart:0,inEnd:10,timeStart:20,
                timeEnd:30,profileHeight:650,profileWidth:950,gain:0.5,method:1,size:20},
            contentType: "application/json;charset=UTF-8", //发送数据的格式
            type: "get",
            dataType: "json", //这是返回来是json，也就是回调json
            success: function(data){
                alert(data.name);
            }
        });
    });
</script>
</body>
</html>
