<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta content="width=device-width,user-scalable=no" name="viewport">
    <title>测试</title>
    <script type="text/javascript" src="${pageContext.request.contextPath }/static/js/jquery-3.1.1.min.js"></script>
</head>
<body style="background:#f3f3f4;" onload="getImg()">
<H2 align="center">测试代码</H2>
<section>
    <div id="Banner" style="border: #0e0e0e 2px solid;">
        <img id="img" width="200px" height="200px" src="">
    </div>
</section>
</body>
</html>
<script>
    function getImg() {
        $.ajax({
            url: "${pageContext.request.contextPath}/img/getImg",
            type: "post",
            async: false, //改为同步
            data: {},
            success: function (result) {
                var imge = result.data;
                console.log(imge);
                document.getElementById("img").src = 'data:image/png;base64,'+imge;
            },
            error: function (result) {
                console.log(result);
            },
        });
    }
</script>

