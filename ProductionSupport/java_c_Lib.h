#pragma once
#include <QString>
#include <QVector>
#ifdef __cplusplus
extern "C" {
#endif

	/*****
功能：扫描函数，获取文件索引
参数：
	path：          数据的文件名
	t_length：		数据总长度
	config：		读取
	返回值：		float *索引数据
	写入成功返回true，否则返回false
******/
	_declspec(dllexport) float * getsegyConf(QString path, int &t_length,float * config ,int configLength);
/*****
功能：通过xline,inline号完成数据道的读取
参数：
	path：          数据的文件名
	xLines：		xline号的集合
	inLines：		inline号的集合
	traceNum：		一共需要多少道
	traceID：		整个数据的道标志
	xlineSize：		xline方向的道数
	t_sampleCount：		单道数据点数
	返回值：float *
	写入成功返回true，否则返回false
******/
	_declspec(dllexport) float * getTraceData(QString path, float * xLines, float * inLines,int traceNum, QVector<int> traceID,int xlineSize, int &t_sampleCount);
	/*****
功能：通过xline,inline号完成数据道的读取
参数：
	path：          数据的文件名
	xLines：		xline号的集合
	inLines：		inline号的集合
	traceID：		整个数据的道标志
	xlineSize：		xline方向的道数
	t_sampleCount：		单道数据点数
	返回值：float *
	写入成功返回true，否则返回false
******/
	_declspec(dllexport) float * getOneTraceData(QString path, int xLines, int inLines,QVector<int> traceID, int xlineSize, int &t_sampleCount);
#ifdef __cplusplus
}
#endif

